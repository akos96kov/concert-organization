INSERT INTO place (id, name, latitude, longitude, address) VALUES
    ('df5536c1-f764-4bae-81ac-c4177d28280b', 'Barba Negra', 47.469355, 19.047463, 'Budapest, Prielle Kornélia u. 4, 1117'),
    ('ef41fa94-2278-4f5f-820a-65e9cc170a92', 'Hipódromo de San Isidro', -34.480765, -58.517849, 'Buenos Aires Province, Argentina');

INSERT INTO media (id, type, path, original_filename) VALUES
    ('eb3cfce0-906f-4a97-afe3-ce27d25ea4f0', 'image/jpeg', 'event/metallica_night.jpg', 'metallica_night.jpg'),
    ('2f94459c-9f7c-4b8c-a611-df2700b5b633', 'image/jpeg', 'event/songs_of_iron.jpg', 'songs_of_iron.jpg'),
    ('6993c925-c514-4ba5-aa9b-fc42ea07f585', 'image/jpeg', 'event/silent_night.jpg', 'silent_night.jpg'),
    ('d2becfd6-f36b-4ea8-a620-4ff4d2c0e53f', 'image/jpeg', 'event/bodomical.jpg', 'bodomical.jpg'),
    ('2f03d07e-d469-4570-92d8-15aa1a7a74fb', 'image/png', 'band/metallica_logo.png', 'metallica_logo.png'),
    ('538f81f5-7477-4481-9ce0-0b5162b132e4', 'image/png', 'band/sex_pistols_logo.png', 'sex_pistols_logo.png'),
    ('9fd51934-c802-4b71-b159-e3432414b306', 'image/png', 'band/dio_logo.png', 'dio_logo.png');

INSERT INTO event (id, name, date, description, thumbnail, place_id) VALUES
    ('f25a9733-7778-401e-8238-c6f3e9030744', 'Metallica night', '2021-10-20 20:00:00', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras congue ipsum a dolor accumsan, quis sollicitudin risus scelerisque. Proin sapien neque, ullamcorper at nisi id, pellentesque consectetur tortor. Nam dapibus aliquam augue, ac maximus metus consectetur tempus.', 'eb3cfce0-906f-4a97-afe3-ce27d25ea4f0', 'df5536c1-f764-4bae-81ac-c4177d28280b'),
    ('afdff277-543e-45ad-9dd9-5cc4b635ed45', 'Songs of Iron', '2021-10-24 20:00:00', 'Nunc fermentum molestie mi a facilisis. Praesent vitae metus sed quam sodales posuere ut in elit. Nam laoreet sem mauris, ultricies fringilla orci tempor ut.', '2f94459c-9f7c-4b8c-a611-df2700b5b633', 'df5536c1-f764-4bae-81ac-c4177d28280b'),
    ('50c23850-7d64-4e14-812a-f30113e1f876', 'Silent Night', '2021-11-10 20:00:00', 'Maecenas eget nibh convallis, tristique lectus in, posuere nibh. Aenean interdum tempus enim eu auctor. Quisque dignissim ante eget porta viverra.', '6993c925-c514-4ba5-aa9b-fc42ea07f585', 'ef41fa94-2278-4f5f-820a-65e9cc170a92'),
    ('d4603480-d4a1-4277-8fcd-91c2d827f32d', 'Bodomical', '2021-12-09 20:00:00', 'Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'd2becfd6-f36b-4ea8-a620-4ff4d2c0e53f', 'ef41fa94-2278-4f5f-820a-65e9cc170a92');

INSERT INTO band (id, name, logo, description) VALUES
    ('0227a2f2-e954-4a89-8c2b-ae4d0a4c0130', 'Metallica', '2f03d07e-d469-4570-92d8-15aa1a7a74fb', null),
    ('ae90193e-e2e2-4600-b6de-82c5dc78ee7a', 'Iron Maiden', null, null),
    ('a9d246c2-e046-44e3-a36c-640943a121a5', 'Disturbed', null, null),
    ('df423b16-0a21-40ca-b679-41f557133046', 'Childre of Bodom', null, null),
    ('f3ff9d38-2e83-4bae-9c04-df459164ff4f', 'Monster Truck', null, null),
    ('697bd7a8-0f6e-4e6b-9556-87f91cfa130b', 'Ivan and the Parazol', null, null),
    ('50a2d1c9-bb9e-475d-8eba-de10b286f4be', 'Guns N Roses', null, null),
    ('c2e10e77-e2d0-4ce3-86c3-23ca1345a7ef', 'Motörhead', null, null),
    ('fbc74974-68b3-4551-bd79-9f5f60813a34', 'Sex Pistols', '538f81f5-7477-4481-9ce0-0b5162b132e4', null),
    ('4e50a24d-06f1-4b24-b9c1-b25de356d9e0', 'Bon Jovi', null, null),
    ('186db697-1258-4c4d-a859-3147ed1b5372', 'Deep Purple', null, null),
    ('fcebe0ce-5f92-48cc-a0b2-12fa08d19b50', 'Whitesnake', null, null),
    ('8c549e82-472a-4163-8253-df5df6c153eb', 'Pantera', null, null),
    ('1bbed0a2-d342-4583-af11-3c27e5177478', 'Oasis', null, null),
    ('d4ffe1b6-99d2-46be-baff-5945735badfc', 'AC/DC', null, 'Az AC/DC ausztrál hard rock zenekart Malcolm és Angus Young testvérek alapították 1973 decemberében, Sydney-ben. Nevük jelentése: váltóáram/egyenáram (az angol alternating current/direct current rövidítése). Az együttes a rock zenei műfaj úttörője, kiemelkedő csapata. Számos tagcsere után 1975-re alakult ki első stabil felállásuk – Malcolm Young ritmusgitáros, Angus Young szólógitáros, Bon Scott énekes, Phil Rudd dobos, Mark Evans basszusgitáros.'),
    ('9fb4363c-42fd-47ff-9ed3-dd5234853540', 'Dio', '9fd51934-c802-4b71-b159-e3432414b306', null);

INSERT INTO band_event (event_id, band_id) VALUES
    ('f25a9733-7778-401e-8238-c6f3e9030744', '0227a2f2-e954-4a89-8c2b-ae4d0a4c0130'),
    ('f25a9733-7778-401e-8238-c6f3e9030744', '697bd7a8-0f6e-4e6b-9556-87f91cfa130b'),
    ('afdff277-543e-45ad-9dd9-5cc4b635ed45', 'ae90193e-e2e2-4600-b6de-82c5dc78ee7a'),
    ('50c23850-7d64-4e14-812a-f30113e1f876', 'a9d246c2-e046-44e3-a36c-640943a121a5'),
    ('d4603480-d4a1-4277-8fcd-91c2d827f32d', 'df423b16-0a21-40ca-b679-41f557133046');

INSERT INTO "user" (id, name, email, username, password, role) VALUES
    ('79e13988-cf48-4991-9fcc-df4e33e82d46', 'Kovács Ákos', 'akos96kov@gmail.com', 'akos', '$2a$04$Mxg5zl9XwzrNznW58foIJezEMSOl3vQO8sqqCbXbIKg2lDrov3Dy2', 'USER'),
    ('b39f39f3-b317-43e7-99d9-cdaed2ba10bb', 'Admin', 'admin@admin.com', 'admin', '$2a$04$HPd3SKDuxyz9R0/J5w6UW.tSdYzNPsZisOLJ4z4PuNeL.yv4xtz2e', 'ADMIN'),
    ('bac2095d-ad38-4a60-9ef0-6134b9b5131d', 'Kiss Péter', 'valaki@valki2.com', 'conductor', '$2a$04$HPd3SKDuxyz9R0/J5w6UW.tSdYzNPsZisOLJ4z4PuNeL.yv4xtz2e', 'CONDUCTOR');

INSERT INTO "ticket" (id, event_id, price, description, quantity) VALUES
    ('41bc58c6-80bc-4902-a61b-3c61be55a950', 'f25a9733-7778-401e-8238-c6f3e9030744', 99.99, 'Seating VIP place.', 200),
    ('312deb3f-bcef-4c25-bea2-71fa23893161', 'f25a9733-7778-401e-8238-c6f3e9030744', 52.99, 'Standing place at the front of the stadium.', 400),
    ('eb547b65-c50c-4e68-868b-9f0e0f25b97e', 'afdff277-543e-45ad-9dd9-5cc4b635ed45', 79.99, 'First sector', 20);
