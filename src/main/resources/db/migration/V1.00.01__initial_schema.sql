-- create table band
CREATE TABLE band (
    id VARCHAR (255) NOT NULL,
    name VARCHAR (255) NOT NULL UNIQUE,
    description TEXT,
    logo VARCHAR (255),
    CONSTRAINT band_primary_key PRIMARY KEY(id)
);

-- create table place
CREATE TABLE place (
    id VARCHAR (255) NOT NULL,
    name VARCHAR (255) NOT NULL UNIQUE,
    latitude FLOAT,
    longitude FLOAT,
    address VARCHAR(255),
    CONSTRAINT place_primary_key PRIMARY KEY (id)
);

-- create table media
CREATE TABLE media (
    id VARCHAR (255) NOT NULL,
    type VARCHAR (50) NOT NULL,
    path TEXT,
    original_filename TEXT,
    CONSTRAINT media_primary_key PRIMARY KEY (id)
);

-- create table event
CREATE TABLE event (
    id VARCHAR (255) NOT NULL,
    name VARCHAR (255) NOT NULL UNIQUE,
    date TIMESTAMP,
    description TEXT,
    thumbnail VARCHAR (255) NOT NULL,
    place_id VARCHAR (255),
    CONSTRAINT event_primary_key PRIMARY KEY (id),
    CONSTRAINT event_place_id_fkey FOREIGN KEY (place_id) REFERENCES place (id)
);

-- create table band_event
CREATE TABLE band_event (
    event_id VARCHAR (255) NOT NULL,
    band_id VARCHAR (255) NOT NULL,
    CONSTRAINT band_event_event_id_fkey FOREIGN KEY (event_id) REFERENCES event (id),
    CONSTRAINT band_event_band_id_fkey FOREIGN KEY (band_id) REFERENCES band (id)
);

-- create table user
CREATE TABLE "user" (
    id VARCHAR (255) NOT NULL,
    name VARCHAR (100) NOT NULL,
    email VARCHAR (50) NOT NULL UNIQUE,
    username VARCHAR (50) NOT NULL UNIQUE,
    password VARCHAR (100) NOT NULL,
    role VARCHAR (50) NOT NULL DEFAULT 'USER',
    CONSTRAINT user_primary_key PRIMARY KEY (id)
);

-- create table ticket
CREATE TABLE "ticket" (
    id VARCHAR (255) NOT NULL,
    event_id VARCHAR (255) NOT NULL,
    price FLOAT NOT NULL,
    description TEXT,
    quantity INTEGER NOT NULL,
    CONSTRAINT ticket_primary_key PRIMARY KEY (id),
    CONSTRAINT ticket_event_id_fkey FOREIGN KEY (event_id) REFERENCES event (id)
);

-- create table user_ticket
CREATE TABLE "cart" (
    user_id VARCHAR (255) NOT NULL,
    ticket_id VARCHAR (255) NOT NULL,
    quantity INTEGER NOT NULL,
    status VARCHAR(15) NOT NULL,
    CONSTRAINT user_ticket_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user" (id),
    CONSTRAINT user_ticket_ticket_id_fkey FOREIGN KEY (ticket_id) REFERENCES ticket (id)
);

-- create table bought_ticket
CREATE TABLE "bought_ticket" (
  user_id VARCHAR (255) NOT NULL,
  ticket_id VARCHAR (255) NOT NULL,
  ticket_code VARCHAR (255) NOT NULL,
  status VARCHAR (20) NOT NULL DEFAULT 'ACTIVE',
  date TIMESTAMP NOT NULL,
  CONSTRAINT bought_ticket_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user" (id),
  CONSTRAINT bought_ticket_ticket_id_fkey FOREIGN KEY (ticket_id) REFERENCES ticket (id)
);
