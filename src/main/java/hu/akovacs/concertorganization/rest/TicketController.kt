package hu.akovacs.concertorganization.rest

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.model.exception.TicketDecreaseFailedException
import hu.akovacs.concertorganization.model.exception.WrongTicketCodeException
import hu.akovacs.concertorganization.service.TicketService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/api/ticket")
class TicketController(private val ticketService: TicketService) {

  @PreAuthorize("hasRole('CONDUCTOR')")
  @PutMapping("/activate/{code}")
  fun activateTicket(@PathVariable("code") ticketCode: String) = ticketService.activateTicket(ticketCode)

  @PreAuthorize("hasRole('CONDUCTOR')")
  @GetMapping("/activate/{code}")
  fun getTicketStatus(@PathVariable("code") ticketCode: String): BoughtTicketStatus = ticketService.getTicketStatus(ticketCode)

  @PreAuthorize("isAuthenticated()")
  @GetMapping
  fun getBoughtTickets(principal: Principal): List<BoughtTicket> = ticketService.getBoughtTickets(principal.name)

  @PreAuthorize("isAuthenticated()")
  @PostMapping("/cart")
  fun addTicketsToCart(@RequestBody ticketQuantities: List<TicketQuantity>, principal: Principal): List<EventUserTicket> {
    return ticketService.addTicketsToCart(ticketQuantities, principal.name)
  }

  @PreAuthorize("isAuthenticated()")
  @PutMapping("cart")
  fun modifyTicketsInCart(@RequestBody ticketQuantitiesForModifying: List<TicketQuantityForModifying>, principal: Principal): List<EventUserTicket> {
    return ticketService.modifyTicketsInCart(ticketQuantitiesForModifying, principal.name)
  }

  @PreAuthorize("isAuthenticated()")
  @DeleteMapping("/cart/{id}")
  fun deleteTicketFromCart(
    @PathVariable("id") ticketId: String,
    principal: Principal,
    @RequestParam(required = true) quantityInCart: Int, ): List<EventUserTicket> {
    return ticketService.deleteTicketFromCart(ticketId, quantityInCart, principal.name)
  }

  @PreAuthorize("isAuthenticated()")
  @PostMapping("/buy")
  fun buyTickets(@RequestBody ticketQuantities: List<TicketQuantityForModifying>, principal: Principal): List<CurrentlyBoughtUserTicket> {
    return ticketService.buyTickets(ticketQuantities, principal.name)
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(TicketDecreaseFailedException::class)
  fun badRequestExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message)
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(WrongTicketCodeException::class)
  fun notFoundExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message)
  }

  companion object {
    private val LOGGER = LoggerFactory.getLogger(TicketController::class.java)
  }
}