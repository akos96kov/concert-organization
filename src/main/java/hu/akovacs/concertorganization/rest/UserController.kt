package hu.akovacs.concertorganization.rest

import hu.akovacs.concertorganization.model.UserDto
import hu.akovacs.concertorganization.model.UserForModify
import hu.akovacs.concertorganization.model.UserWithCartTickets
import hu.akovacs.concertorganization.model.exception.*
import hu.akovacs.concertorganization.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid


@RestController
@RequestMapping("/api/user")
class UserController(private val userService: UserService) {

  @GetMapping
  @PreAuthorize("isAuthenticated()")
  fun isAuthenticated(principal: Principal): UserWithCartTickets {
    return userService.getUserDetailsByUsername(principal.name)
  }

  @PutMapping
  @PreAuthorize("isAuthenticated()")
  fun modifyUser(principal: Principal, @RequestBody userForModify: @Valid UserForModify) {
    return userService.modifyUser(userForModify, principal.name)
  }

  @PostMapping("/register")
  fun register(@RequestBody userDto: @Valid UserDto) {
    userService.register(userDto)
  }

  @ResponseStatus(HttpStatus.CONFLICT)
  @ExceptionHandler(
    UserAlreadyExistsException::class,
    WrongUserPrincipalsException::class,
    UsernameOrEmailIsReserved::class)
  fun conflictExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message)
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(UserNotFoundException::class)
  fun notFoundExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message)
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(RegistrationFailedException::class)
  fun badRequestExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message)
  }

  companion object {
    private val LOGGER = LoggerFactory.getLogger(UserController::class.java)
  }
}
