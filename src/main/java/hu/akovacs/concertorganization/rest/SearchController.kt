package hu.akovacs.concertorganization.rest

import hu.akovacs.concertorganization.model.SearchResultDto
import hu.akovacs.concertorganization.service.SearchService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/search")
class SearchController(private val searchService: SearchService) {

    @GetMapping
    fun fullTextSearch(@RequestParam(required = false) searchText: String?): SearchResultDto = searchService.fullTextSearch(searchText)
}
