package hu.akovacs.concertorganization.rest

import hu.akovacs.concertorganization.model.PlaceDto
import hu.akovacs.concertorganization.model.PlaceForSave
import hu.akovacs.concertorganization.model.exception.PlaceNameAlreadyExistsException
import hu.akovacs.concertorganization.service.PlaceService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/place")
class PlaceController(private val placeService: PlaceService) {

  @GetMapping
  fun getPlaces(): List<PlaceDto> = placeService.getPlaces()

  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/{id}/admin")
  fun getPlaceById(@PathVariable("id") id: String): PlaceDto = placeService.getPlaceById(id)

  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/admin")
  fun savePlace(@RequestBody @Valid place: PlaceForSave) = placeService.savePlace(place)

  @PreAuthorize("hasRole('ADMIN')")
  @PutMapping("/admin")
  fun modifyPlace(@RequestBody @Valid place: PlaceDto) = placeService.modifyPlace(place)

  @ResponseStatus(HttpStatus.CONFLICT)
  @ExceptionHandler(PlaceNameAlreadyExistsException::class)
  fun conflictExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message);
  }

  companion object {
    private val LOGGER = LoggerFactory.getLogger(PlaceController::class.java)
  }
}
