package hu.akovacs.concertorganization.rest;

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.model.exception.EventNameAlreadyExistsException
import hu.akovacs.concertorganization.model.exception.EventNotFoundException
import hu.akovacs.concertorganization.model.exception.InvalidEventDateException
import hu.akovacs.concertorganization.service.EventService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.validation.Valid

@RestController
@RequestMapping("/api/event")
class EventController(
  private val eventService: EventService) {

  @GetMapping
  fun getEvents(): List<EventDto> = eventService.getEvents(null, false)

  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/admin")
  fun getEventsForAdmin(): List<EventDto> = eventService.getEvents(null, true)

  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/{id}/admin")
  fun getEventForAdmin(@PathVariable("id") eventId: String): EventForAdmin = eventService.getEventForAdmin(eventId)

  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/admin")
  fun saveEvent(
    @RequestPart("event-thumbnail", required = false) eventThumbnail: MultipartFile,
    @RequestPart("event") @Valid event: EventForSave) {
    eventService.saveEvent(eventThumbnail, event)
  }

  @PreAuthorize("hasRole('ADMIN')")
  @PutMapping("/admin")
  fun modifyEvent(
    @RequestPart("event-thumbnail", required = false) eventThumbnail: MultipartFile?,
    @RequestPart("event") @Valid event: EventForModify) {
    eventService.modifyEvent(eventThumbnail, event)
  }

  @GetMapping("{id}")
  fun getEventById(@PathVariable("id") eventId: String): EventDto = eventService.getEventById(eventId)

  @GetMapping("/forthComing")
  fun getForthComingEvents(): List<EventSimpleDto> = eventService.getForthComingEvents()

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(EventNotFoundException::class)
  fun notFoundExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message)
  }

  @ResponseStatus(HttpStatus.CONFLICT)
  @ExceptionHandler(EventNameAlreadyExistsException::class)
  fun conflictExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(InvalidEventDateException::class)
  fun badRequestExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message);
  }

  companion object {
    private val LOGGER = LoggerFactory.getLogger(EventController::class.java)
  }
}
