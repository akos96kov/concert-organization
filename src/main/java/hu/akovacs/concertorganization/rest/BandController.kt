package hu.akovacs.concertorganization.rest;

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.model.exception.BandNameAlreadyExistsException
import hu.akovacs.concertorganization.model.exception.BandNotFoundException
import hu.akovacs.concertorganization.service.BandService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.validation.Valid

@RestController
@RequestMapping("/api/band")
class BandController(
  private val bandService: BandService
) {

  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/{id}/admin")
  fun getBandForAdmin(@PathVariable("id") bandId: String): BandForAdmin {
    return bandService.getBandForAdmin(bandId)
  }

  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/admin")
  fun saveBand(
    @RequestPart("band-logo", required = false) bandLogo: MultipartFile?,
    @RequestPart("band") @Valid band: BandForSave) {
    bandService.saveBand(band, bandLogo)
  }

  @PreAuthorize("hasRole('ADMIN')")
  @PutMapping("/admin")
  fun modifyBand(
    @RequestPart("band-logo", required = false) bandLogo: MultipartFile?,
    @RequestPart("band") @Valid band: BandForModify) {
    bandService.modifyBand(band, bandLogo)
  }

  @GetMapping("/{id}")
  fun getBandById(@PathVariable("id") bandId: String): BandWithEventsDto {
    return bandService.getBandById(bandId)
  }

  @GetMapping
  fun getBands(): List<BandDto> {
    return bandService.getBands(null)
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(BandNotFoundException::class)
  fun notFoundExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message);
  }

  @ResponseStatus(HttpStatus.CONFLICT)
  @ExceptionHandler(BandNameAlreadyExistsException::class)
  fun conflictExceptionHandler(ex: Exception) {
    LOGGER.error(ex.message);
  }

  companion object {
    private val LOGGER = LoggerFactory.getLogger(BandController::class.java)
  }
}
