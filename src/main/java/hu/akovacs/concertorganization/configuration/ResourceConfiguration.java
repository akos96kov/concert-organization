package hu.akovacs.concertorganization.configuration;

import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Configuration
public class ResourceConfiguration implements WebMvcConfigurer {

    private ResourceProperties resourceProperties;

    public ResourceConfiguration(ResourceProperties resourceProperties) {
        this.resourceProperties = resourceProperties;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations(resourceProperties.getStaticLocations())
                .setCacheControl(resourceProperties.getCache().getCachecontrol().toHttpCacheControl())
                .resourceChain(resourceProperties.getChain().isCache())
                .addResolver(new SPAResourceResolver());
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }
}

class SPAResourceResolver extends PathResourceResolver {

    @Override
    public Resource resolveResource(HttpServletRequest request, String requestPath, List<? extends Resource> locations, ResourceResolverChain chain) {
        Resource resource = super.resolveResource(request, requestPath, locations, chain);

        if (resource != null) {
            return resource;
        } else {
            return super.resolveResource(request, "/index.html", locations, chain);
        }
    }
}
