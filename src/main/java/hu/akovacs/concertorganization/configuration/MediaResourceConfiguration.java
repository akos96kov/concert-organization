package hu.akovacs.concertorganization.configuration;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
//@EnableConfigurationProperties(MediaConfigProperties.class)
public class MediaResourceConfiguration implements WebMvcConfigurer {

  @Value("${config.media}")
  private String media;

  // TODO: get from properties
  private final String mediaDirectory = "c:/work/cor-media/";

  public MediaResourceConfiguration() {
    final Path mediaDirectoryPath = Paths.get(mediaDirectory);

    if (!Files.exists(mediaDirectoryPath) || !Files.isDirectory(mediaDirectoryPath)) {
      throw new BeanCreationException("Media directory must be a valid directory: $mediaDirectory");
    }
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry
      .addResourceHandler("/media/**")
      .addResourceLocations("file:" + mediaDirectory);
  }
}
