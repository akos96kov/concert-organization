package hu.akovacs.concertorganization.repository;

import hu.akovacs.concertorganization.model.PlaceDto
import hu.akovacs.concertorganization.model.PlaceForSave
import hu.akovacs.concertorganization.persistence.model.tables.Place.PLACE
import org.jooq.DSLContext
import org.springframework.stereotype.Repository

@Repository
class PlaceRepository(private val database: DSLContext) {

  fun getPlaces(): List<PlaceDto> =
    database.select(
      PLACE.ID,
      PLACE.NAME,
      PLACE.ADDRESS,
      PLACE.LATITUDE,
      PLACE.LONGITUDE)
      .from(PLACE)
      .orderBy(PLACE.NAME)
      .fetch()
      .map {
        PlaceDto(
          id = it[PLACE.ID],
          name = it[PLACE.NAME],
          address = it[PLACE.ADDRESS],
          latitude = it[PLACE.LATITUDE],
          longitude = it[PLACE.LONGITUDE]
        )
      }

  fun getPlaceById(id: String): PlaceDto =
    database.select(
      PLACE.ID,
      PLACE.NAME,
      PLACE.ADDRESS,
      PLACE.LATITUDE,
      PLACE.LONGITUDE)
      .from(PLACE)
      .where(PLACE.ID.eq(id))
      .fetchOne()
      .map {
        PlaceDto(
          id = it[PLACE.ID],
          name = it[PLACE.NAME],
          address = it[PLACE.ADDRESS],
          latitude = it[PLACE.LATITUDE],
          longitude = it[PLACE.LONGITUDE]
        )
      }

  fun savePlace(place: PlaceForSave, placeId: String) =
    database.insertInto(PLACE)
      .columns(
        PLACE.ID,
        PLACE.NAME,
        PLACE.ADDRESS,
        PLACE.LATITUDE,
        PLACE.LONGITUDE)
      .values(
        placeId,
        place.name,
        place.address,
        place.latitude,
        place.longitude)
      .execute()

  fun modifyPlace(place: PlaceDto) =
    database.update(PLACE)
      .set(PLACE.NAME, place.name)
      .set(PLACE.ADDRESS, place.address)
      .set(PLACE.LATITUDE, place.latitude)
      .set(PLACE.LONGITUDE, place.longitude)
      .where(PLACE.ID.eq(place.id))
      .execute()

  fun isPlaceExistsByName(placeName: String) =
    database.fetchExists(
      database.select()
        .from(PLACE)
        .where(PLACE.NAME.eq(placeName))
    )

  fun isPlaceExistsByNameAndId(placeName: String, placeId: String) =
    database.fetchExists(
      database.select()
        .from(PLACE)
        .where(PLACE.NAME.eq(placeName))
        .and(PLACE.ID.notEqual(placeId))
    )
}
