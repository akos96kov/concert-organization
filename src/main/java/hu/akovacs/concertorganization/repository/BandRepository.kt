package hu.akovacs.concertorganization.repository

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.model.exception.BandNotFoundException
import hu.akovacs.concertorganization.persistence.model.tables.Band.BAND
import hu.akovacs.concertorganization.persistence.model.tables.BandEvent
import hu.akovacs.concertorganization.persistence.model.tables.Media.MEDIA
import org.jooq.Condition
import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.springframework.stereotype.Repository

@Repository
class BandRepository(private val database: DSLContext) {

  fun getBandsByEventId(eventId: String): List<BandDto> =
    database.select(
      BAND.ID,
      BAND.NAME,
      BAND.LOGO,
      BAND.DESCRIPTION,
      MEDIA.TYPE,
      MEDIA.PATH,
      MEDIA.ORIGINAL_FILENAME)
      .from(BAND)
      .leftJoin(BandEvent.BAND_EVENT).on(BandEvent.BAND_EVENT.BAND_ID.eq(BAND.ID))
      .leftJoin(MEDIA).on(MEDIA.ID.eq(BAND.LOGO))
      .where(BandEvent.BAND_EVENT.EVENT_ID.eq(eventId))
      .fetch()
      .map {
        var logo: MediaDto? = null
        if (it[BAND.LOGO] !== null) {
          logo = MediaDto(
            type = it[MEDIA.TYPE],
            path = it[MEDIA.PATH],
            fileName = it[MEDIA.ORIGINAL_FILENAME]
          )
        }
        BandDto(
          id = it[BAND.ID],
          name = it[BAND.NAME],
          logo = logo,
          description = it[BAND.DESCRIPTION]
        )
      }

  fun getBandIdsByEventId(eventId: String): List<String> =
    database.select(
      BandEvent.BAND_EVENT.BAND_ID)
      .from(BandEvent.BAND_EVENT)
      .where(BandEvent.BAND_EVENT.EVENT_ID.eq(eventId))
      .fetch()
      .map {
        it[BandEvent.BAND_EVENT.BAND_ID]
      }

  fun getBandForAdmin(bandId: String): BandForAdmin =
    database.select(
      BAND.ID,
      BAND.NAME,
      BAND.LOGO,
      BAND.DESCRIPTION,
      MEDIA.PATH)
      .from(BAND)
      .leftJoin(MEDIA).on(MEDIA.ID.eq(BAND.LOGO))
      .where(BAND.ID.eq(bandId))
      .fetchOne()
      .map {
        val logo = if (it[BAND.LOGO] !== null) {
          it[MEDIA.PATH]
        } else {
          null
        }
        BandForAdmin(
          id = it[BAND.ID],
          name = it[BAND.NAME],
          logo = logo,
          description = it[BAND.DESCRIPTION]
        )
      }

  fun getBandById(bandId: String): BandDto =
    database.select(
      BAND.ID,
      BAND.NAME,
      BAND.LOGO,
      BAND.DESCRIPTION,
      MEDIA.TYPE,
      MEDIA.PATH,
      MEDIA.ORIGINAL_FILENAME)
      .from(BAND)
      .leftJoin(MEDIA).on(MEDIA.ID.eq(BAND.LOGO))
      .where(BAND.ID.eq(bandId))
      .fetchOptional()
      .map {
        var logo: MediaDto? = null
        if (it[BAND.LOGO] !== null) {
          logo = MediaDto(
            type = it[MEDIA.TYPE],
            path = it[MEDIA.PATH],
            fileName = it[MEDIA.ORIGINAL_FILENAME]
          )
        }
        BandDto(
          id = it[BAND.ID],
          name = it[BAND.NAME],
          logo = logo,
          description = it[BAND.DESCRIPTION]
        )
      }.orElseThrow { BandNotFoundException("Band not found with id: $bandId") }

  fun getBands(searchText: String?): List<BandDto> {
    var condition: Condition = DSL.trueCondition()

    if (!searchText.isNullOrEmpty()) {
      condition = condition.and(BAND.NAME.likeIgnoreCase("%$searchText%"))
    }

    return database.select(
      BAND.ID,
      BAND.NAME,
      BAND.LOGO,
      BAND.DESCRIPTION,
      MEDIA.TYPE,
      MEDIA.PATH,
      MEDIA.ORIGINAL_FILENAME)
      .from(BAND)
      .leftJoin(MEDIA).on(MEDIA.ID.eq(BAND.LOGO))
      .where(condition)
      .orderBy(BAND.NAME)
      .fetch()
      .map {
        var logo: MediaDto? = null
        if (it[BAND.LOGO] !== null) {
          logo = MediaDto(
            type = it[MEDIA.TYPE],
            path = it[MEDIA.PATH],
            fileName = it[MEDIA.ORIGINAL_FILENAME]
          )
        }
        BandDto(
          id = it[BAND.ID],
          name = it[BAND.NAME],
          logo = logo,
          description = it[BAND.DESCRIPTION]
        )
      }
  }

  fun saveBand(id: String, band: BandForSave, logo: String?) =
    database.insertInto(BAND)
      .columns(
        BAND.ID,
        BAND.NAME,
        BAND.LOGO,
        BAND.DESCRIPTION)
      .values(
        id,
        band.name,
        logo,
        band.description)
      .execute()

  fun modifyBand(band: BandForModify, logo: String?) =
    database.update(BAND)
      .set(BAND.NAME, band.name)
      .set(BAND.LOGO, logo)
      .set(BAND.DESCRIPTION, band.description)
      .where(BAND.ID.eq(band.id))
      .execute()

  fun getBandLogoById(id: String): String? =
    database.select(BAND.LOGO)
      .from(BAND)
      .where((BAND.ID.eq(id)))
      .fetchOne()
      .map {
        it[BAND.LOGO]
      }

  fun isBandExistsByName(bandName: String) =
    database.fetchExists(
      database.select()
        .from(BAND)
        .where(BAND.NAME.eq(bandName))
    )

  fun isBandExistsByNameAndId(bandName: String, bandId: String) =
    database.fetchExists(
      database.select()
        .from(BAND)
        .where(BAND.NAME.eq(bandName))
        .and(BAND.ID.notEqual(bandId))
    )

}
