package hu.akovacs.concertorganization.repository

import hu.akovacs.concertorganization.model.MediaDto
import hu.akovacs.concertorganization.persistence.model.tables.Media.MEDIA
import org.jooq.DSLContext
import org.springframework.stereotype.Repository

@Repository
class MediaRepository(private val database: DSLContext) {

  fun addMedia(uuid: String, mimeType: String, filePath: String, originalFilename: String) = database
    .insertInto(MEDIA)
    .columns(MEDIA.ID, MEDIA.TYPE, MEDIA.PATH, MEDIA.ORIGINAL_FILENAME)
    .values(uuid, mimeType, filePath, originalFilename)
    .execute()

  fun getMedia(id: String): MediaDto? = database
    .selectFrom(MEDIA)
    .where(MEDIA.ID.eq(id))
    .fetchOne()
    .map {
      MediaDto(
        type = it[MEDIA.TYPE],
        path = it[MEDIA.PATH],
        fileName = it[MEDIA.ORIGINAL_FILENAME]
      )
    }


  fun removeMedia(id: String) = database
    .deleteFrom(MEDIA)
    .where(MEDIA.ID.eq(id))
    .execute()
}

