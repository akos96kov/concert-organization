package hu.akovacs.concertorganization.repository

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.model.exception.EventNotFoundException
import hu.akovacs.concertorganization.persistence.model.tables.BandEvent.BAND_EVENT
import hu.akovacs.concertorganization.persistence.model.tables.Event.EVENT
import hu.akovacs.concertorganization.persistence.model.tables.Media
import hu.akovacs.concertorganization.persistence.model.tables.Place
import org.jooq.Condition
import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.springframework.stereotype.Repository
import java.sql.Timestamp
import java.time.LocalDateTime

@Repository
class EventRepository(private val database: DSLContext) {

  fun getEvents(searchText: String?, futureEventsOnly: Boolean?): List<EventEntity> {
    var condition: Condition = DSL.trueCondition()

    if (!searchText.isNullOrEmpty()) {
      condition = condition
        .and(EVENT.NAME.likeIgnoreCase("%$searchText%"))
        .or(EVENT.DESCRIPTION.likeIgnoreCase("%$searchText%"))
    }

    if (futureEventsOnly == true) {
      condition = condition
        .and(EVENT.DATE.greaterThan(Timestamp.valueOf(LocalDateTime.now())))
    }

    return database.select(
      EVENT.ID,
      EVENT.NAME,
      EVENT.DATE,
      EVENT.DESCRIPTION,
      EVENT.THUMBNAIL,
      Media.MEDIA.TYPE,
      Media.MEDIA.PATH,
      Media.MEDIA.ORIGINAL_FILENAME,
      Place.PLACE.ID,
      Place.PLACE.NAME,
      Place.PLACE.LATITUDE,
      Place.PLACE.LONGITUDE,
      Place.PLACE.ADDRESS)
      .from(EVENT)
      .innerJoin(Media.MEDIA).on(Media.MEDIA.ID.eq(EVENT.THUMBNAIL))
      .innerJoin(Place.PLACE).on(Place.PLACE.ID.eq(EVENT.PLACE_ID))
      .where(condition)
      .fetch()
      .map {
        EventEntity(
          it[EVENT.ID],
          it[EVENT.NAME],
          it[EVENT.DATE].toLocalDateTime(),
          it[EVENT.DESCRIPTION],
          MediaDto(
            it[Media.MEDIA.TYPE],
            it[Media.MEDIA.PATH],
            it[Media.MEDIA.ORIGINAL_FILENAME]
          ),
          PlaceDto(
            it[Place.PLACE.ID],
            it[Place.PLACE.NAME],
            it[Place.PLACE.LATITUDE],
            it[Place.PLACE.LONGITUDE],
            it[Place.PLACE.ADDRESS]
          )
        )
      }
  }

  fun getEventForAdmin(eventId: String): EventForAdmin =
    database.select(
      EVENT.ID,
      EVENT.NAME,
      EVENT.DATE,
      EVENT.DESCRIPTION,
      EVENT.THUMBNAIL,
      Media.MEDIA.PATH,
      Place.PLACE.ID)
      .from(EVENT)
      .innerJoin(Media.MEDIA).on(Media.MEDIA.ID.eq(EVENT.THUMBNAIL))
      .innerJoin(Place.PLACE).on(Place.PLACE.ID.eq(EVENT.PLACE_ID))
      .where(EVENT.ID.eq(eventId))
      .fetchOne()
      .map {
        EventForAdmin(
          id = it[EVENT.ID],
          name = it[EVENT.NAME],
          date = it[EVENT.DATE].toLocalDateTime(),
          description = it[EVENT.DESCRIPTION],
          thumbnail = it[Media.MEDIA.PATH],
          placeId = it[Place.PLACE.ID]
        )
      }

  fun saveEvent(event: EventForSave, thumbnail: String, id: String) =
    database.insertInto(EVENT)
      .columns(
        EVENT.ID,
        EVENT.NAME,
        EVENT.DATE,
        EVENT.DESCRIPTION,
        EVENT.THUMBNAIL,
        EVENT.PLACE_ID)
      .values(
        id,
        event.name,
        Timestamp.valueOf(event.date),
        event.description,
        thumbnail,
        event.placeId)
      .execute()

  fun modifyEvent(event: EventForModify, thumbnail: String) =
    database.update(EVENT)
      .set(EVENT.NAME, event.name)
      .set(EVENT.DESCRIPTION, event.description)
      .set(EVENT.DATE, Timestamp.valueOf(event.date))
      .set(EVENT.THUMBNAIL, thumbnail)
      .set(EVENT.PLACE_ID, event.placeId)
      .where(EVENT.ID.eq(event.id))
      .execute()

  fun deleteBandEvent(eventId: String) =
    database.delete(BAND_EVENT)
      .where(BAND_EVENT.EVENT_ID.eq(eventId))
      .execute()

  fun getEventThumbnailId(eventId: String): String =
    database.select(EVENT.THUMBNAIL)
      .from(EVENT)
      .where(EVENT.ID.eq(eventId))
      .fetchOne()
      .map {
        it[EVENT.THUMBNAIL]
      }

  fun saveBandForEvent(eventId: String, bandId: String) =
    database.insertInto(BAND_EVENT)
      .columns(BAND_EVENT.BAND_ID, BAND_EVENT.EVENT_ID)
      .values(bandId, eventId)
      .execute()

  fun getEventsByBandId(bandId: String): List<EventEntity> =
    database.select(
      EVENT.ID,
      EVENT.NAME,
      EVENT.DATE,
      EVENT.DESCRIPTION,
      EVENT.THUMBNAIL,
      Media.MEDIA.TYPE,
      Media.MEDIA.PATH,
      Media.MEDIA.ORIGINAL_FILENAME,
      Place.PLACE.ID,
      Place.PLACE.NAME,
      Place.PLACE.LATITUDE,
      Place.PLACE.LONGITUDE,
      Place.PLACE.ADDRESS)
      .from(EVENT)
      .innerJoin(BAND_EVENT).on(BAND_EVENT.EVENT_ID.eq(EVENT.ID))
      .innerJoin(Media.MEDIA).on(Media.MEDIA.ID.eq(EVENT.THUMBNAIL))
      .innerJoin(Place.PLACE).on(Place.PLACE.ID.eq(EVENT.PLACE_ID))
      .where(BAND_EVENT.BAND_ID.eq(bandId))
      .fetch()
      .map {
        EventEntity(
          it[EVENT.ID],
          it[EVENT.NAME],
          it[EVENT.DATE].toLocalDateTime(),
          it[EVENT.DESCRIPTION],
          MediaDto(
            it[Media.MEDIA.TYPE],
            it[Media.MEDIA.PATH],
            it[Media.MEDIA.ORIGINAL_FILENAME]
          ),
          PlaceDto(
            it[Place.PLACE.ID],
            it[Place.PLACE.NAME],
            it[Place.PLACE.LATITUDE],
            it[Place.PLACE.LONGITUDE],
            it[Place.PLACE.ADDRESS]
          )
        )
      }

  fun getEventById(eventId: String): EventEntity =
    database.select(
      EVENT.ID,
      EVENT.NAME,
      EVENT.DATE,
      EVENT.DESCRIPTION,
      EVENT.THUMBNAIL,
      Media.MEDIA.TYPE,
      Media.MEDIA.PATH,
      Media.MEDIA.ORIGINAL_FILENAME,
      Place.PLACE.ID,
      Place.PLACE.NAME,
      Place.PLACE.LATITUDE,
      Place.PLACE.LONGITUDE,
      Place.PLACE.ADDRESS)
      .from(EVENT)
      .innerJoin(Media.MEDIA).on(Media.MEDIA.ID.eq(EVENT.THUMBNAIL))
      .innerJoin(Place.PLACE).on(Place.PLACE.ID.eq(EVENT.PLACE_ID))
      .where(EVENT.ID.eq(eventId))
      .fetchOptional()
      .map {
        EventEntity(
          it[EVENT.ID],
          it[EVENT.NAME],
          it[EVENT.DATE].toLocalDateTime(),
          it[EVENT.DESCRIPTION],
          MediaDto(
            it[Media.MEDIA.TYPE],
            it[Media.MEDIA.PATH],
            it[Media.MEDIA.ORIGINAL_FILENAME]
          ),
          PlaceDto(
            it[Place.PLACE.ID],
            it[Place.PLACE.NAME],
            it[Place.PLACE.LATITUDE],
            it[Place.PLACE.LONGITUDE],
            it[Place.PLACE.ADDRESS]
          )
        )
      }.orElseThrow { EventNotFoundException("Event not found with id: $eventId") }

  fun getForthComingEvents(): List<EventSimpleDto> =
    database.select(
      EVENT.ID,
      EVENT.NAME,
      Media.MEDIA.TYPE,
      Media.MEDIA.PATH,
      Media.MEDIA.ORIGINAL_FILENAME)
      .from(EVENT)
      .innerJoin(Media.MEDIA).on(Media.MEDIA.ID.eq(EVENT.THUMBNAIL))
      .orderBy(EVENT.DATE)
      .limit(3)
      .fetch()
      .map {
        EventSimpleDto(
          it[EVENT.ID],
          it[EVENT.NAME],
          MediaDto(
            it[Media.MEDIA.TYPE],
            it[Media.MEDIA.PATH],
            it[Media.MEDIA.ORIGINAL_FILENAME]
          )

        )
      }

  fun isEventExistsByName(eventName: String) =
    database.fetchExists(
      database.select()
        .from(EVENT)
        .where(EVENT.NAME.eq(eventName))
    )

  fun isEventExistsByNameAndId(eventName: String, eventId: String) =
    database.fetchExists(
      database.select()
        .from(EVENT)
        .where(EVENT.NAME.eq(eventName))
        .and(EVENT.ID.notEqual(eventId))
    )
}
