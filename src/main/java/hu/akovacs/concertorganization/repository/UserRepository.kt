package hu.akovacs.concertorganization.repository

import hu.akovacs.concertorganization.model.UserDetails
import hu.akovacs.concertorganization.model.UserEntity
import hu.akovacs.concertorganization.model.UserForModify
import hu.akovacs.concertorganization.model.exception.UserNotFoundException
import hu.akovacs.concertorganization.persistence.model.tables.User.USER
import org.jooq.DSLContext
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Repository


@Repository
class UserRepository(private val database: DSLContext) {

  fun getUserByUsername(username: String): UserEntity = database.select().from(USER)
    .where(USER.USERNAME.eq(username))
    .fetchOptional()
    .map {
      UserEntity(
        it[USER.ID],
        it[USER.NAME],
        it[USER.EMAIL],
        it[USER.USERNAME],
        it[USER.PASSWORD],
        it[USER.ROLE])
    }.orElseThrow { UsernameNotFoundException("User not found with username: $username") }

  fun getUserDetailsByUsername(username: String): UserDetails =
    database.select().from(USER)
      .where(USER.USERNAME.eq(username))
      .fetchOptional()
      .map {
        UserDetails(
          it[USER.ID],
          it[USER.NAME],
          it[USER.EMAIL],
          it[USER.USERNAME],
          it[USER.ROLE])
      }.orElseThrow { UserNotFoundException("User not found with username: $username.") }

  fun isUserExists(email: String, username: String): Boolean =
    database.fetchExists(
      database.select()
        .from(USER)
        .where(USER.EMAIL.eq(email))
        .or(USER.USERNAME.eq(username))
    )

  fun createUser(user: UserEntity) {
    database
      .insertInto(USER)
      .columns(USER.ID, USER.NAME, USER.EMAIL, USER.USERNAME, USER.PASSWORD)
      .values(user.id, user.name, user.email, user.username, user.password)
      .execute()
  }

  fun modifyUser(user: UserForModify) {
    database
      .update(USER)
      .set(USER.NAME, user.name)
      .set(USER.EMAIL, user.email)
      .set(USER.USERNAME, user.username)
      .where(USER.ID.eq(user.id))
      .execute()
  }

  fun isUsernameOrEmailReserved(user: UserForModify): Boolean =
    database.fetchExists(
      database.select()
        .from(USER)
        .where(USER.EMAIL.eq(user.email))
        .or(USER.USERNAME.eq(user.username))
        .and(USER.ID.notEqual(user.id))
    )
}
