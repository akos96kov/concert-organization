package hu.akovacs.concertorganization.repository

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.model.exception.TicketDecreaseFailedException
import hu.akovacs.concertorganization.model.exception.WrongTicketCodeException
import hu.akovacs.concertorganization.persistence.model.Tables.*
import hu.akovacs.concertorganization.persistence.model.tables.Event.EVENT
import hu.akovacs.concertorganization.persistence.model.tables.Ticket.TICKET
import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.springframework.stereotype.Repository
import java.sql.Timestamp

@Repository
class TicketRepository(private val database: DSLContext) {

  fun getTicketsByEventId(eventId: String): List<Ticket> =
    database.select(
      TICKET.ID,
      TICKET.PRICE,
      TICKET.DESCRIPTION,
      TICKET.QUANTITY)
      .from(TICKET)
      .where(TICKET.EVENT_ID.eq(eventId))
      .fetch()
      .map {
        Ticket(
          id = it[TICKET.ID],
          price = it[TICKET.PRICE],
          description = it[TICKET.DESCRIPTION],
          quantity = it[TICKET.QUANTITY]
        )
      }

  fun decreaseTicketQuantity(quantity: Int, ticketId: String) {
    val modifiedRecords = database.update(TICKET)
      .set(TICKET.QUANTITY, TICKET.QUANTITY - quantity)
      .where(TICKET.ID.eq(ticketId))
      .and(TICKET.QUANTITY.greaterOrEqual(quantity))
      .execute()

    if (modifiedRecords == 0) {
      throw TicketDecreaseFailedException("Cannot decrease ticket quantity with id: ${ticketId}")
    }
  }

  fun enlargeUserTicketQunatity(quantity: Int, ticketId: String, userId: String) =
    database.update(CART)
      .set(CART.QUANTITY, CART.QUANTITY + quantity)
      .where(CART.USER_ID.eq(userId))
      .and(CART.TICKET_ID.eq(ticketId))
      .and(CART.STATUS.eq(UserTicketStatus.CART.name))
      .execute()

  fun deleteUserTicketFromCart(ticketId: String, userId: String) =
    database.delete(CART)
      .where(CART.USER_ID.eq(userId))
      .and(CART.TICKET_ID.eq(ticketId))
      .and(CART.STATUS.eq(UserTicketStatus.CART.name))
      .execute()

  fun deleteAllUserTicketFromCart(userId: String) =
    database.delete(CART)
      .where(CART.USER_ID.eq(userId))
      .and(CART.STATUS.eq(UserTicketStatus.CART.name))
      .execute()

  fun insertToCart(ticketId: String, quantity: Int, userId: String, status: UserTicketStatus) =
    database.insertInto(CART)
      .columns(
        CART.USER_ID,
        CART.TICKET_ID,
        CART.QUANTITY,
        CART.STATUS)
      .values(
        userId,
        ticketId,
        quantity,
        status.name)
      .execute()

  fun getEventUserTickets(userId: String): List<EventUserTicket> =
    database.select(
      CART.TICKET_ID,
      CART.QUANTITY,
      TICKET.QUANTITY,
      TICKET.DESCRIPTION,
      TICKET.PRICE,
      EVENT.NAME)
      .from(CART)
      .innerJoin(TICKET).on(TICKET.ID.eq(CART.TICKET_ID))
      .innerJoin(EVENT).on(EVENT.ID.eq(TICKET.EVENT_ID))
      .where(CART.USER_ID.eq(userId))
      .and(CART.STATUS.eq(UserTicketStatus.CART.name))
      .fetch()
      .map {
        EventUserTicket(
          ticketId = it[CART.TICKET_ID],
          quantityInCart = it[CART.QUANTITY],
          originalQuantityInCart = it[CART.QUANTITY],
          quantity = it[TICKET.QUANTITY],
          eventName = it[EVENT.NAME],
          ticketDescription = it[TICKET.DESCRIPTION],
          ticketPrice = it[TICKET.PRICE]
        )
      }

  fun insertBoughtTicket(ticketId: String, userId: String, date: Timestamp, ticketCode: String) =
    database
      .insertInto(BOUGHT_TICKET)
      .columns(
        BOUGHT_TICKET.USER_ID,
        BOUGHT_TICKET.TICKET_ID,
        BOUGHT_TICKET.TICKET_CODE,
        BOUGHT_TICKET.DATE)
      .values(
        userId,
        ticketId,
        ticketCode,
        date)
      .execute()

  fun getCurrentlyBoughtEventUserTickets(userId: String, date: Timestamp): List<CurrentlyBoughtUserTicket> =
    database.select(
      BOUGHT_TICKET.TICKET_ID,
      DSL.count().`as`(TICKET_QUANTITY),
      TICKET.DESCRIPTION,
      EVENT.NAME)
      .from(BOUGHT_TICKET)
      .innerJoin(TICKET).on(TICKET.ID.eq(BOUGHT_TICKET.TICKET_ID))
      .innerJoin(EVENT).on(EVENT.ID.eq(TICKET.EVENT_ID))
      .where(BOUGHT_TICKET.USER_ID.eq(userId))
      .and(BOUGHT_TICKET.DATE.eq(date))
      .groupBy(BOUGHT_TICKET.TICKET_ID, TICKET.DESCRIPTION, EVENT.NAME)
      .fetch()
      .map {
        CurrentlyBoughtUserTicket(
          quantity = it[TICKET_QUANTITY] as Int,
          eventName = it[EVENT.NAME],
          ticketDescription = it[TICKET.DESCRIPTION],
        )
      }

  fun activateBoughtTicket(ticketCode: String) =
    database.update(BOUGHT_TICKET)
      .set(BOUGHT_TICKET.STATUS, BoughtTicketStatus.USED.name)
      .where(BOUGHT_TICKET.TICKET_CODE.eq(ticketCode))
      .and(BOUGHT_TICKET.STATUS.eq(BoughtTicketStatus.ACTIVE.name))
      .execute()

  fun getTicketStatus(ticketCode: String): BoughtTicketStatus =
    database.select(BOUGHT_TICKET.STATUS)
      .from(BOUGHT_TICKET)
      .where(BOUGHT_TICKET.TICKET_CODE.eq(ticketCode))
      .fetchOptional()
      .map {
        BoughtTicketStatus.valueOf(it[BOUGHT_TICKET.STATUS])
      }
      .orElseThrow { WrongTicketCodeException("Wrong ticket code: $ticketCode}") }

  fun getBoughtTickets(userId: String): List<BoughtTicket> =
    database.select(
      BOUGHT_TICKET.TICKET_CODE,
      BOUGHT_TICKET.DATE,
      TICKET.DESCRIPTION,
      TICKET.PRICE,
      EVENT.NAME,
      EVENT.DATE,
      EVENT.DESCRIPTION,
      PLACE.NAME)
      .from(BOUGHT_TICKET)
      .innerJoin(TICKET).on(TICKET.ID.eq(BOUGHT_TICKET.TICKET_ID))
      .innerJoin(EVENT).on(EVENT.ID.eq(TICKET.EVENT_ID))
      .innerJoin(PLACE).on(PLACE.ID.eq(EVENT.PLACE_ID))
      .where(BOUGHT_TICKET.USER_ID.eq(userId))
      .orderBy(EVENT.DATE)
      .fetch()
      .map {
        BoughtTicket(
          ticketCode = it[BOUGHT_TICKET.TICKET_CODE],
          ticketDescription = it[TICKET.DESCRIPTION],
          eventName = it[EVENT.NAME],
          date = it[BOUGHT_TICKET.DATE].toLocalDateTime(),
          eventDate = it[EVENT.DATE].toLocalDateTime(),
          placeName = it[PLACE.NAME],
          price = it[TICKET.PRICE],
          eventDescription = it[EVENT.DESCRIPTION]
        )
      }

  fun saveTicket(ticket: Ticket, eventId: String) =
    database.insertInto(TICKET)
      .columns(
        TICKET.ID,
        TICKET.EVENT_ID,
        TICKET.DESCRIPTION,
        TICKET.QUANTITY,
        TICKET.PRICE)
      .values(
        ticket.id,
        eventId,
        ticket.description,
        ticket.quantity,
        ticket.price)
      .execute()

  fun modifyTicket(ticket: Ticket) =
    database.update(TICKET)
      .set(TICKET.PRICE, ticket.price)
      .set(TICKET.QUANTITY, ticket.quantity)
      .set(TICKET.DESCRIPTION, ticket.description)
      .where(TICKET.ID.eq(ticket.id))
      .execute()

  fun deleteTickets(eventId: String) =
    database.delete(TICKET)
      .where(TICKET.EVENT_ID.eq(eventId))
      .execute()

  companion object {
    const val TICKET_QUANTITY = "TICKET_QUANTITY"
  }
}
