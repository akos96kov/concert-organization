package hu.akovacs.concertorganization.service;

import hu.akovacs.concertorganization.model.CustomUser;
import hu.akovacs.concertorganization.model.UserEntity;
import hu.akovacs.concertorganization.repository.UserRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    UserEntity userEntity = userRepository.getUserByUsername(username);

    return new CustomUser(
      userEntity.getUsername(),
      userEntity.getPassword(),
      new ArrayList<>(),
      userEntity.getId(),
      userEntity.getRole());
  }
}
