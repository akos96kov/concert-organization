package hu.akovacs.concertorganization.service;

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.model.exception.EventNameAlreadyExistsException
import hu.akovacs.concertorganization.model.exception.InvalidEventDateException
import hu.akovacs.concertorganization.repository.BandRepository
import hu.akovacs.concertorganization.repository.EventRepository
import hu.akovacs.concertorganization.repository.TicketRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.time.LocalDateTime
import java.util.*

@Service
class EventService(
  private val eventRepository: EventRepository,
  private val bandRepository: BandRepository,
  private val ticketRepository: TicketRepository,
  private val mediaService: MediaService) {

  @Transactional(readOnly = true)
  fun getEvents(searchText: String?, futureEventsOnly: Boolean): List<EventDto> {
    val eventEntities = eventRepository.getEvents(searchText, futureEventsOnly)

    return eventEntities.map {
      val bandDtos = bandRepository.getBandsByEventId(it.id)

      EventDto(
        id = it.id,
        name = it.name,
        date = it.date,
        description = it.description,
        thumbnail = it.thumbnail,
        place = it.place,
        bandList = bandDtos
      )
    }
  }

  @Transactional(readOnly = true)
  fun getEventForAdmin(eventId: String): EventForAdmin {
    val event = eventRepository.getEventForAdmin(eventId)
    val bandIds = bandRepository.getBandIdsByEventId(eventId)
    val tickets = ticketRepository.getTicketsByEventId(eventId)

    event.bandIdList = bandIds
    event.ticketList = tickets

    return event
  }

  @Transactional(readOnly = true)
  fun getEventById(eventId: String): EventDto {
    val eventEntity = eventRepository.getEventById(eventId)
    val bands = bandRepository.getBandsByEventId(eventId)
    val tickets = ticketRepository.getTicketsByEventId(eventId)

    return EventDto(
      eventEntity.id,
      eventEntity.name,
      eventEntity.date,
      eventEntity.description,
      eventEntity.thumbnail,
      eventEntity.place,
      bands,
      tickets
    )
  }

  @Transactional(readOnly = true)
  fun getForthComingEvents(): List<EventSimpleDto> = eventRepository.getForthComingEvents()

  @Transactional
  fun saveEvent(eventThumbnail: MultipartFile, event: EventForSave) {
    if (eventRepository.isEventExistsByName(event.name)) {
      throw EventNameAlreadyExistsException("The event with name '${event.name}' already exists")
    }

    val mediaId = mediaService.uploadMedia(file = eventThumbnail, targetMap = "event")

    val eventId = UUID.randomUUID().toString()
    eventRepository.saveEvent(event, mediaId, eventId)

    for (bandId in event.bandIdList) {
      eventRepository.saveBandForEvent(eventId, bandId)
    }

    for (ticket in event.ticketList) {
      val ticketId = UUID.randomUUID().toString()
      ticket.id = ticketId

      ticketRepository.saveTicket(ticket, eventId)
    }
  }

  @Transactional
  fun modifyEvent(eventThumbnail: MultipartFile?, event: EventForModify) {
    if (eventRepository.isEventExistsByNameAndId(event.name, event.id)) {
      throw EventNameAlreadyExistsException("The event with name '${event.name}' already exists")
    }

    if (event.date.isBefore(LocalDateTime.now())) {
      throw InvalidEventDateException("The date ${event.date} is invalid for the event")
    }

    var mediaId = eventRepository.getEventThumbnailId(event.id)

    if (eventThumbnail !== null) {
      mediaService.removeMedia(mediaId)
      mediaId = mediaService.uploadMedia(file = eventThumbnail, targetMap = "event")
    }

    eventRepository.deleteBandEvent(event.id)
    eventRepository.modifyEvent(event, mediaId)

    for (bandId in event.bandIdList) {
      eventRepository.saveBandForEvent(event.id, bandId)
    }

//    ticketRepository.deleteTickets(event.id)
    for (ticket in event.ticketList) {
      if (ticket.id.isEmpty()) {
        val ticketId = UUID.randomUUID().toString()
        ticket.id = ticketId

        ticketRepository.saveTicket(ticket, event.id)
      } else {
        ticketRepository.modifyTicket(ticket)
      }
    }

//    TODO: Send email
  }
}
