package hu.akovacs.concertorganization.service

import hu.akovacs.concertorganization.model.PlaceDto
import hu.akovacs.concertorganization.model.PlaceForSave
import hu.akovacs.concertorganization.model.exception.PlaceNameAlreadyExistsException
import hu.akovacs.concertorganization.repository.PlaceRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class PlaceService(private val placeRepository: PlaceRepository) {

  @Transactional(readOnly = true)
  fun getPlaces(): List<PlaceDto> = placeRepository.getPlaces()

  @Transactional(readOnly = true)
  fun getPlaceById(id: String): PlaceDto = placeRepository.getPlaceById(id)

  @Transactional
  fun savePlace(place: PlaceForSave) {
    if (placeRepository.isPlaceExistsByName(place.name)) {
      throw PlaceNameAlreadyExistsException("The place with name '${place.name}' already exists")
    }

    val placeId = UUID.randomUUID().toString()
    placeRepository.savePlace(place, placeId)
  }

  @Transactional
  fun modifyPlace(place: PlaceDto) {
    if (placeRepository.isPlaceExistsByNameAndId(place.name, place.id)) {
      throw PlaceNameAlreadyExistsException("The place with name '${place.name}' already exists")
    }

    placeRepository.modifyPlace(place)
  }
}
