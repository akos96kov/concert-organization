package hu.akovacs.concertorganization.service

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.repository.TicketRepository
import hu.akovacs.concertorganization.repository.UserRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

@Service
class TicketService(
  private val ticketRepository: TicketRepository,
  private val userRepository: UserRepository) {

  @Transactional
  fun addTicketsToCart(ticketQuantities: List<TicketQuantity>, username: String): List<EventUserTicket> {
    val user = userRepository.getUserByUsername(username)

    for (ticketQuantity in ticketQuantities) {
      ticketRepository.decreaseTicketQuantity(ticketQuantity.quantity, ticketQuantity.ticketId)

      val modifiedUserTicketRecords = ticketRepository.enlargeUserTicketQunatity(
        ticketQuantity.quantity,
        ticketQuantity.ticketId,
        user.id
      )

      if (modifiedUserTicketRecords == 0) {
        ticketRepository.insertToCart(ticketQuantity.ticketId, ticketQuantity.quantity, user.id, UserTicketStatus.CART)
      }
    }

    return ticketRepository.getEventUserTickets(user.id)
  }

  @Transactional
  fun modifyTicketsInCart(ticketQuantities: List<TicketQuantityForModifying>, username: String): List<EventUserTicket> {
    val user = userRepository.getUserByUsername(username)

    for (ticketQuantity in ticketQuantities) {
      if (ticketQuantity.quantityInCart == 0) {
        ticketRepository.deleteUserTicketFromCart(ticketQuantity.ticketId, user.id)
      } else {
        ticketRepository.enlargeUserTicketQunatity(
          ticketQuantity.quantityInCart - ticketQuantity.originalQuantityInCart,
          ticketQuantity.ticketId,
          user.id
        )
      }

      ticketRepository.decreaseTicketQuantity(
        ticketQuantity.quantityInCart - ticketQuantity.originalQuantityInCart,
        ticketQuantity.ticketId
      )
    }

    return ticketRepository.getEventUserTickets(user.id)
  }

  @Transactional
  fun deleteTicketFromCart(ticketId: String, quantityInCart: Int, username: String): List<EventUserTicket> {
    val user = userRepository.getUserByUsername(username)

    ticketRepository.decreaseTicketQuantity(-quantityInCart, ticketId)

    ticketRepository.deleteUserTicketFromCart(ticketId, user.id)

    return ticketRepository.getEventUserTickets(user.id)
  }

  @Transactional
  fun buyTickets(ticketQuantities: List<TicketQuantityForModifying>, username: String): List<CurrentlyBoughtUserTicket> {
    val user = userRepository.getUserByUsername(username)

    ticketRepository.deleteAllUserTicketFromCart(user.id)

    val currentDate = Timestamp.valueOf(LocalDateTime.now())

    for (ticketQuantity in ticketQuantities) {
      if (ticketQuantity.quantityInCart != ticketQuantity.originalQuantityInCart) {
        ticketRepository.decreaseTicketQuantity(
          ticketQuantity.quantityInCart - ticketQuantity.originalQuantityInCart,
          ticketQuantity.ticketId
        )
      }

      for (quantity in 1..ticketQuantity.quantityInCart) {
        val ticketCode = UUID.randomUUID().toString()
        ticketRepository.insertBoughtTicket(ticketQuantity.ticketId, user.id, currentDate, ticketCode)
      }
    }

    return ticketRepository.getCurrentlyBoughtEventUserTickets(user.id, currentDate)
  }

  @Transactional(readOnly = true)
  fun getBoughtTickets(username: String): List<BoughtTicket> {
    val user = userRepository.getUserByUsername(username)

    return ticketRepository.getBoughtTickets(user.id)
  }

  @Transactional
  fun activateTicket(ticketCode: String) {
    ticketRepository.activateBoughtTicket(ticketCode)
  }

  @Transactional(readOnly = true)
  fun getTicketStatus(ticketCode: String): BoughtTicketStatus {
    return ticketRepository.getTicketStatus(ticketCode)
  }
}
