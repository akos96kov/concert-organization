package hu.akovacs.concertorganization.service

import hu.akovacs.concertorganization.mail.EmailService
import hu.akovacs.concertorganization.model.UserDto
import hu.akovacs.concertorganization.model.UserEntity
import hu.akovacs.concertorganization.model.UserForModify
import hu.akovacs.concertorganization.model.UserWithCartTickets
import hu.akovacs.concertorganization.model.exception.RegistrationFailedException
import hu.akovacs.concertorganization.model.exception.UserAlreadyExistsException
import hu.akovacs.concertorganization.model.exception.UsernameOrEmailIsReserved
import hu.akovacs.concertorganization.model.exception.WrongUserPrincipalsException
import hu.akovacs.concertorganization.repository.TicketRepository
import hu.akovacs.concertorganization.repository.UserRepository
import org.springframework.mail.MailSendException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class UserService(
  private val userRepository: UserRepository,
  private val passwordEncoder: PasswordEncoder,
  private val ticketRepository: TicketRepository,
  private val emailService: EmailService) {

  @Transactional
  fun register(userDto: UserDto) {
    val isUserExists = userRepository.isUserExists(userDto.email, userDto.username)

    if (isUserExists) {
      throw UserAlreadyExistsException("User already exists")
    }

    val userId = UUID.randomUUID().toString()
    val hashedPassword = passwordEncoder.encode(userDto.password)

    val user = UserEntity(
      userId,
      userDto.name,
      userDto.email,
      userDto.username,
      hashedPassword,
      "USER"
    )

    userRepository.createUser(user)

    try {
      emailService.sendSimpleMessage(userDto.email, "regisztráció", "Sikeres regisztráció!")
    } catch (ex: MailSendException) {
      throw RegistrationFailedException("Failed to send email to: ${userDto.email} address.")
    }
  }

  fun getUserDetailsByUsername(username: String): UserWithCartTickets {

    val userDetails = userRepository.getUserDetailsByUsername(username)
    val ticketQuantities = ticketRepository.getEventUserTickets(userDetails.id)

    return UserWithCartTickets(
      id = userDetails.id,
      name = userDetails.name,
      email = userDetails.email,
      username = userDetails.username,
      role = userDetails.role,
      eventUserTickets = ticketQuantities
    )
  }

  @Transactional
  fun modifyUser(user: UserForModify, username: String) {
    if (username != user.originalUsername) {
      throw WrongUserPrincipalsException("wrong user principals!")
    }

    if (userRepository.isUsernameOrEmailReserved(user)) {
      throw UsernameOrEmailIsReserved("username or email is reserved!")
    }

    userRepository.modifyUser(user)
  }
}
