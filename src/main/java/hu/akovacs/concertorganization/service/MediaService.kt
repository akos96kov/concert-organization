package hu.akovacs.concertorganization.service

import hu.akovacs.concertorganization.repository.MediaRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

@Service
class MediaService(private val mediaRepository: MediaRepository) {
  private val mediaPath = Paths.get("C:\\work\\cor-media")

  @Transactional
  fun uploadMedia(parentId: String? = null, file: MultipartFile, targetMap: String): String {
    val uuid = UUID.randomUUID().toString()
    val filename = generateFilename(uuid, file.contentType, file.originalFilename)
    val filePath = assembleFilePath(targetMap, parentId, filename)
    val fullPath = assembleFullPath(filePath)

    Files.createDirectories(fullPath.parent)
    file.transferTo(fullPath)

    mediaRepository.addMedia(uuid, file.contentType
      ?: "image/*", filePath.toString().replace("\\", "/"), file.originalFilename ?: "")
    return uuid
  }

  @Transactional
  fun removeMedia(id: String) {
    mediaRepository.getMedia(id)?.let {
      mediaRepository.removeMedia(id)
      removeMediaFile(it.path)
    }
  }

  private fun removeMediaFile(filePath: String) {
    val absolutePath = assembleFullPathFromString(filePath)

    Files.deleteIfExists(absolutePath)
  }

  private fun generateFilename(id: String, mimeType: String?, originalFilename: String?): String {
    return extractExtension(mimeType, originalFilename)?.let { extension ->
      "$id.$extension"
    } ?: id
  }

  private fun extractExtension(mimeType: String?, originalFilename: String?): String? {
    return extractExtensionFromString(originalFilename, filenameExtensionRegex)
      ?: extractExtensionFromString(mimeType, mimeTypeExtensionRegex)
  }

  private fun extractExtensionFromString(from: String?, usingRegex: Regex): String? {
    return from?.let {
      usingRegex.matchEntire(it)
    }?.let {
      it.groupValues[1]
    }
  }

  private fun assembleFilePath(type: String, parentId: String? = null, filename: String): Path =
    Paths.get(type.toLowerCase())
      .let { path ->
        parentId?.let {
          path.resolve(parentId)
        } ?: path
      }
      .resolve(filename)

  private fun assembleFullPathFromString(filePath: String) =
    mediaPath.resolve(filePath)

  private fun assembleFullPath(filePath: Path) =
    mediaPath.resolve(filePath)

  companion object {
    private val filenameExtensionRegex = Regex("^.+\\.([^.]+)$")
    private val mimeTypeExtensionRegex = Regex("^.+/([^/*]+)$")
  }
}