package hu.akovacs.concertorganization.service;

import hu.akovacs.concertorganization.model.*
import hu.akovacs.concertorganization.model.exception.BandNameAlreadyExistsException
import hu.akovacs.concertorganization.repository.BandRepository
import hu.akovacs.concertorganization.repository.EventRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.util.*

@Service
class BandService(
  private val bandRepository: BandRepository,
  private val eventRepository: EventRepository,
  private val mediaService: MediaService) {

  @Transactional(readOnly = true)
  fun getBandForAdmin(bandId: String): BandForAdmin {
    return bandRepository.getBandForAdmin(bandId)
  }

  @Transactional
  fun saveBand(band: BandForSave, bandLogo: MultipartFile?) {
    if (bandRepository.isBandExistsByName(band.name)) {
      throw BandNameAlreadyExistsException("The band with name '${band.name}' already exists")
    }

    val mediaId = if (bandLogo !== null) {
      mediaService.uploadMedia(file = bandLogo, targetMap = "band")
    } else {
      null
    }

    val bandId = UUID.randomUUID().toString()

    bandRepository.saveBand(bandId, band, mediaId)
  }

  @Transactional
  fun modifyBand(band: BandForModify, bandLogo: MultipartFile?) {
    if (bandRepository.isBandExistsByNameAndId(band.name, band.id)) {
      throw BandNameAlreadyExistsException("The band with name '${band.name}' already exists")
    }

    var mediaId: String? = bandRepository.getBandLogoById(band.id)

    if (bandLogo === null) {
      if (band.removeLogo && mediaId !== null) {
        mediaService.removeMedia(mediaId)
        mediaId = null
      }
    } else {
      if (band.removeLogo && mediaId !== null) {
        mediaService.removeMedia(mediaId)
      }
      mediaId = mediaService.uploadMedia(file = bandLogo, targetMap = "band")
    }

    bandRepository.modifyBand(band, mediaId)
  }

  @Transactional(readOnly = true)
  fun getBandById(bandId: String): BandWithEventsDto {
    val bandDto = bandRepository.getBandById(bandId)
    val eventEntities = eventRepository.getEventsByBandId(bandId);

    return BandWithEventsDto(
      name = bandDto.name,
      logo = bandDto.logo,
      description = bandDto.description,
      events = eventEntities
    )
  }

  @Transactional(readOnly = true)
  fun getBands(searchText: String?): List<BandDto> = bandRepository.getBands(searchText)
}
