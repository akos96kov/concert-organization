package hu.akovacs.concertorganization.service

import hu.akovacs.concertorganization.model.SearchResultDto
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class SearchService(
  private val eventService: EventService,
  private val bandService: BandService) {

  @Transactional(readOnly = true)
  fun fullTextSearch(searchText: String?): SearchResultDto {
    val events = eventService.getEvents(searchText, false)
    val bands = bandService.getBands(searchText)

    return SearchResultDto(
      events = events,
      bands = bands
    )
  }
}