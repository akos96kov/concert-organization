package hu.akovacs.concertorganization.model;

import hu.akovacs.concertorganization.service.passwordvalidator.PasswordMatches;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@PasswordMatches
public class UserDto {

  @NotNull
  @NotEmpty
  private String name;

  @Email
  @NotNull
  @NotEmpty
  private String email;

  @NotNull
  @NotEmpty
  private String username;

  @NotNull
  @NotEmpty
  private String password;

  private String passwordConfirm;

  public UserDto() {
  }

  public UserDto(
    String name,
    String email,
    String username,
    String password,
    String passwordConfirm) {
    this.name = name;
    this.email = email;
    this.username = username;
    this.password = password;
    this.passwordConfirm = passwordConfirm;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }

  public String getPasswordConfirm() {
    return passwordConfirm;
  }

  public void setPasswordConfirm(final String passwordConfirm) {
    this.passwordConfirm = passwordConfirm;
  }
}
