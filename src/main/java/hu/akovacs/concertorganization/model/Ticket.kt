package hu.akovacs.concertorganization.model

import java.time.LocalDateTime

data class Ticket(
  var id: String,
  val price: Double,
  val quantity: Int,
  val description: String
)

data class TicketQuantity(
  val ticketId: String,
  val quantity: Int
)

data class TicketQuantityForModifying(
  val ticketId: String,
  val quantityInCart: Int,
  val originalQuantityInCart: Int
)

data class CurrentlyBoughtUserTicket(
  val quantity: Int,
  val ticketDescription: String,
  val eventName: String,
)

data class BoughtTicket(
  val ticketCode: String,
  val ticketDescription: String,
  val eventName: String,
  val eventDate: LocalDateTime,
  val eventDescription: String,
  val date: LocalDateTime,
  val placeName: String,
  val price: Double
)

data class EventUserTicket(
  val ticketId: String,
  val quantityInCart: Int,
  val originalQuantityInCart: Int,
  val quantity: Int,
  val eventName: String,
  val ticketDescription: String,
  val ticketPrice: Double
)

enum class UserTicketStatus {
  CART,
  BOUGHT
}

enum class BoughtTicketStatus {
  ACTIVE,
  USED
}
