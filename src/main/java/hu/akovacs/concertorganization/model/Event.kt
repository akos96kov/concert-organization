package hu.akovacs.concertorganization.model;

import java.time.LocalDateTime
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class EventDto(
  val id: String,
  val name: String,
  val date: LocalDateTime,
  val description: String?,
  val thumbnail: MediaDto?,
  val place: PlaceDto,
  val bandList: List<BandDto>,
  val ticketList: List<Ticket>? = null
)

data class EventSimpleDto(
  val id: String,
  val name: String,
  val thumbnail: MediaDto
)

data class EventEntity(
  val id: String,
  val name: String,
  val date: LocalDateTime,
  val description: String?,
  val thumbnail: MediaDto,
  val place: PlaceDto
)

data class EventForAdmin(
  var id: String,
  val name: String,
  val date: LocalDateTime,
  val description: String?,
  var thumbnail: String,
  val placeId: String,
  var bandIdList: List<String>? = null,
  var ticketList: List<Ticket>? = null
)

data class EventForModify(
  @field:NotNull
  @field:NotEmpty
  var id: String,
  @field:NotNull
  @field:NotEmpty
  val name: String,
  @field:NotNull
  val date: LocalDateTime,
  val description: String?,
  @field:NotNull
  @field:NotEmpty
  val placeId: String,
  @field:NotNull
  var bandIdList: List<String>,
  @field:NotNull
  var ticketList: List<Ticket>,
  val removeThumbnail: Boolean
)

data class EventForSave(
  @field:NotNull
  @field:NotEmpty
  val name: String,
  @field:NotNull
  val date: LocalDateTime,
  val description: String?,
  @field:NotNull
  @field:NotEmpty
  val placeId: String,
  @field:NotNull
  var bandIdList: List<String>,
  @field:NotNull
  var ticketList: List<Ticket>
)
