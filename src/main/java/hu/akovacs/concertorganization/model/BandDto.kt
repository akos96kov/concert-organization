package hu.akovacs.concertorganization.model;

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class BandDto(
  val id: String,
  val name: String,
  val logo: MediaDto?,
  val description: String?
)

data class BandWithEventsDto(
  val name: String,
  val logo: MediaDto?,
  val description: String?,
  val events: List<EventEntity>
)

data class BandForAdmin(
  val id: String,
  val name: String,
  val logo: String?,
  val description: String?
)

data class BandForSave(
  @field:NotNull
  @field:NotEmpty
  val name: String,
  val description: String?
)

data class BandForModify(
  @field:NotNull
  @field:NotEmpty
  val id: String,
  @field:NotNull
  @field:NotEmpty
  val name: String,
  val removeLogo: Boolean,
  val description: String?,
)
