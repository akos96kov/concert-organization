package hu.akovacs.concertorganization.model;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private final String jwttoken;
    private final String username;

    public JwtResponse(String jwttoken, String username) {
        this.jwttoken = jwttoken;
        this.username = username;
    }

    public String getToken() {
        return jwttoken;
    }

    public String getUsername() {
        return username;
    }
}
