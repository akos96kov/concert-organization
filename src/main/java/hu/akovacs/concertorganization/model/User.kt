package hu.akovacs.concertorganization.model

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class UserWithCartTickets(
  val id: String,
  val name: String,
  val email: String,
  val username: String,
  val eventUserTickets: List<EventUserTicket>,
  val role: String
)

data class UserForModify(
  @field:NotNull
  @field:NotEmpty
  val id: String,
  @field:NotNull
  @field:NotEmpty
  val name: String,
  @field:NotNull
  @field:NotEmpty
  val email: String,
  @field:NotNull
  @field:NotEmpty
  val username: String,
  @field:NotNull
  @field:NotEmpty
  val originalUsername: String
)
