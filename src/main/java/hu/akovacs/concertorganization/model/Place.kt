package hu.akovacs.concertorganization.model

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class PlaceDto(
  @field:NotNull
  @field:NotEmpty
  val id: String,
  @field:NotNull
  @field:NotEmpty
  val name: String,
  @field:NotNull
  val latitude: Double,
  @field:NotNull
  val longitude: Double,
  @field:NotNull
  @field:NotEmpty
  val address: String
)

data class PlaceForSave(
  @field:NotNull
  @field:NotEmpty
  val name: String,
  @field:NotNull
  val latitude: Double,
  @field:NotNull
  val longitude: Double,
  @field:NotNull
  @field:NotEmpty
  val address: String
)
