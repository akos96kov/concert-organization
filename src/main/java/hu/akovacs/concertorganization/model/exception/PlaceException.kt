package hu.akovacs.concertorganization.model.exception

class PlaceNameAlreadyExistsException(message: String) : RuntimeException(message)
