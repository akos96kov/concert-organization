package hu.akovacs.concertorganization.model.exception

class BandNotFoundException(message: String) : RuntimeException(message)

class BandNameAlreadyExistsException(message: String) : RuntimeException(message)
