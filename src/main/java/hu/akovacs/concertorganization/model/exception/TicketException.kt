package hu.akovacs.concertorganization.model.exception

class TicketNotFoundException(message: String) : RuntimeException(message)

class TicketDecreaseFailedException(message: String) : RuntimeException(message)

class WrongTicketCodeException(message: String) : RuntimeException(message)
