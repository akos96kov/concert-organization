package hu.akovacs.concertorganization.model.exception

class EventNotFoundException(message: String) : RuntimeException(message)

class EventNameAlreadyExistsException(message: String) : RuntimeException(message)

class InvalidEventDateException(message: String) : RuntimeException(message)
