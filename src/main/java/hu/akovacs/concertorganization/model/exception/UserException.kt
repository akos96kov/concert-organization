package hu.akovacs.concertorganization.model.exception

class UserAlreadyExistsException(message: String) : RuntimeException(message)

class UserNotFoundException(message: String) : RuntimeException(message)

class RegistrationFailedException(message: String) : RuntimeException(message)

class WrongUserPrincipalsException(message: String) : RuntimeException(message)

class UsernameOrEmailIsReserved(message: String) : RuntimeException(message)
