package hu.akovacs.concertorganization.model

data class MediaDto (
        val type: String,
        val path: String,
        val fileName: String
)
