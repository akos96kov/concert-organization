package hu.akovacs.concertorganization.model

data class SearchResultDto (
    val events: List<EventDto>,
    val bands: List<BandDto>
)
