package hu.akovacs.concertorganization.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CustomUser extends User {

  private final String ROLE_PREFIX = "ROLE_";

  private String role;
  private final String userId;

  public CustomUser(
    String username,
    String password,
    Collection<? extends GrantedAuthority> authorities,
    String userId,
    String role) {
    super(username, password, authorities);
    this.userId = userId;
    this.role = role;
  }

  @Override
  public Collection<GrantedAuthority> getAuthorities() {
    List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();

    list.add(new SimpleGrantedAuthority(ROLE_PREFIX + role));

    return list;
  }

}
