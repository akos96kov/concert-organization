package hu.akovacs.concertorganization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConcertOrganizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConcertOrganizationApplication.class, args);
	}

}
