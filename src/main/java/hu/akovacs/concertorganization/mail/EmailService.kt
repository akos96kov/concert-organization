package hu.akovacs.concertorganization.mail

import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Component

@Component
class EmailService(private val emailSender: JavaMailSender) {

  fun sendSimpleMessage(to: String, subject: String, text: String) {
    val message = SimpleMailMessage()

    message.setFrom("noreply@baeldung.com")
    message.setTo(to)
    message.setSubject(subject)
    message.setText(text)

    emailSender.send(message)
  }
}
