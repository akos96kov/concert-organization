import React, { useState, useEffect } from 'react';

import EventCard from '../../components/Event/EventCard';

import { bandResource } from '../../rest/resource';

const Band = ({ match: { params }, history }) => {
  const [band, setBand] = useState();

  useEffect(() => {
    bandResource
      .getBand(params.id)
      .then((fetchedBand) => {
        setBand(fetchedBand);
      })
      .catch(() => {
        history.replace('/');
      });
  }, []);

  if (!band) {
    return null;
  }

  return (
    <div className="main">
      <div className="main-column band-page">
        {band.logo && (
          <div className="band-logo-container">
            <img className="band-logo" alt="band-logo" src={`/media/${band.logo.path}`} />
          </div>
        )}
        <div className="band-name-container">{band.name}</div>
        {band.description && <div className="band-description-container">{band.description}</div>}
        {!!band.events.length && (
          <div className="band-events-container">
            <span className="band-events-title">Események:</span>
            {band.events.map((event) => (
              <EventCard
                onCardClick={() => history.push(`/event/${event.id}`)}
                key={event.id}
                name={event.name}
                thumbnail={event.thumbnail}
                date={event.date}
                place={event.place}
                description={event.description}
              />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Band;
