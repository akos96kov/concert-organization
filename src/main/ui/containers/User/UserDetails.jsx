import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import PersonIcon from '@material-ui/icons/Person';
import QRCode from 'qrcode.react';

import InputField from '../../components/InputField/InputField';
import Card from '../../components/Card/Card';
import Button from '../../components/Button/Button';
import ModalTicket from '../../components/Modal/ModalTicket';
import { ticketResource, userResource } from '../../rest/resource';
import { dateFormatter } from '../../utils/commonFunctions';
import { toggleShowingInfoPopup } from '../../store/actions/popupActions';
import { emptyUser } from '../../store/actions/userActions';

const UserDetails = ({ history }) => {
  const user = useSelector((state) => state.user.user);
  const [boughtTickets, setBoughtTickets] = useState([]);
  const [originalBoughtTickets, setOriginalBoughtTickets] = useState();
  const [selectedTicket, setSelectedTicet] = useState();
  const [dataChanged, setDataChanged] = useState(false);
  const [onlyFutureEvents, setOnlyFutureEvents] = useState(false);
  const dispatch = useDispatch();

  if (!user) {
    return <Redirect to="/" />;
  }

  useState(() => {
    ticketResource.fetchBoughtTickets().then((fetchedBoughtTickets) => {
      setBoughtTickets(fetchedBoughtTickets);
      setOriginalBoughtTickets(fetchedBoughtTickets);
    });
  }, []);

  const [name, setName] = useState(user.name);
  const [nameError, setNameError] = useState(false);
  const [email, setEmail] = useState(user.email);
  const [emailError, setEmailError] = useState(false);
  const [username, setUsername] = useState(user.username);
  const [originalUsername, setOriginalUsername] = useState(user.username);
  const [usernameError, setUsernameError] = useState(false);

  const onTicketSelect = (ticket) => {
    const qrCodeCanvas = document.getElementById(ticket.ticketCode);
    const qrCodeDataUri = qrCodeCanvas.toDataURL('image/jpg', 0.3);

    setSelectedTicet({ ...ticket, qrCode: qrCodeDataUri });
  };

  const onTicketClose = () => {
    setSelectedTicet(null);
  };

  const handleEventDateFilterChange = () => {
    const today = new Date();

    let filteredBoughtTickets = [...originalBoughtTickets];

    if (!onlyFutureEvents) {
      filteredBoughtTickets = originalBoughtTickets.filter(
        (originalBoughtTicket) =>
          today.getTime() < new Date(originalBoughtTicket.eventDate).getTime()
      );
    }

    setOnlyFutureEvents((prevOnlyFutureEvens) => !prevOnlyFutureEvens);
    setBoughtTickets(filteredBoughtTickets);
  };

  const onNameChange = (e) => {
    if (!dataChanged) {
      setDataChanged(true);
    }

    setName(e.target.value);
  };

  const onEmailChange = (e) => {
    if (!dataChanged) {
      setDataChanged(true);
    }

    setEmail(e.target.value);
  };

  const onUsernameChange = (e) => {
    if (!dataChanged) {
      setDataChanged(true);
    }

    setUsername(e.target.value);
  };

  const saveUserData = async () => {
    let error = false;

    if (!name) {
      error = true;
      setNameError(true);
    }

    if (!email) {
      error = true;
      setEmailError(true);
    }

    if (!username) {
      error = true;
      setUsernameError(true);
    }

    if (error) {
      dispatch(toggleShowingInfoPopup('Hiba a kitöltésben!', 'ERROR'));
    } else {
      const userForModify = {
        id: user.id,
        name,
        username,
        originalUsername,
        email,
      };

      try {
        await userResource.modifyUser(userForModify);
        dispatch(toggleShowingInfoPopup('Sikeres adat módosítás!', 'SUCCESS'));
        localStorage.clear();
        history.replace('/');
        dispatch(emptyUser());
      } catch (err) {
        dispatch(toggleShowingInfoPopup('Valami hiba történt!', 'ERROR'));
      }
    }
  };

  return (
    <div className="main user-details-page">
      <div className="main-column">
        {selectedTicket && (
          <ModalTicket onClose={onTicketClose} ticket={selectedTicket} />
        )}
        <div className="user-data">
          <div className="user-thumbnai-default">
            <PersonIcon />
          </div>
          <InputField
            name="name"
            title="teljes név"
            value={name}
            onChange={onNameChange}
            error={nameError}
          />
          <InputField
            name="email"
            title="e-mail cím"
            value={email}
            onChange={onEmailChange}
            error={emailError}
          />
          <InputField
            name="username"
            title="felhasználónév"
            value={username}
            onChange={onUsernameChange}
            error={usernameError}
          />
          <Button contained disabled={!dataChanged} onClick={saveUserData}>
            Adatok mentése
          </Button>
        </div>

        {!!boughtTickets.length && (
          <div className="bought-ticket-list">
            <div className="bought-tickets-header">
              <div className="bought-tickets-title">Megvásárolt jegyek</div>
              <div className="bought-tickets-filter-container">
                <span>Csak jövőbeli események</span>
                <input
                  type="checkbox"
                  checked={onlyFutureEvents}
                  onChange={handleEventDateFilterChange}
                />
              </div>
            </div>
            {boughtTickets.map((boughtTicket, index) => (
              <div key={index}>
                <div className="ticket-bought-date">
                  Megvásárolva: {dateFormatter(boughtTicket.date, true)}
                </div>
                <Card
                  key={index}
                  className="bought-ticket-card"
                  handleCardClick={() => onTicketSelect(boughtTicket)}
                >
                  <div className="ticket">
                    <div className="ticket-details-container">
                      <div className="event-name">{boughtTicket.eventName}</div>
                      <div className="event-date-place-block">
                        {boughtTicket.placeName} / {dateFormatter(boughtTicket.eventDate)}
                      </div>
                      <div className="ticket-description">{boughtTicket.ticketDescription}</div>
                    </div>
                    <div className="ticket-price">${boughtTicket.price}</div>
                  </div>
                </Card>
                <QRCode
                  id={boughtTicket.ticketCode}
                  className="bought-ticket-qr-code"
                  value={`${window.location.origin}/ticket/${boughtTicket.ticketCode}`}
                  size={200}
                  bgColor="#ffffff"
                  fgColor="#000000"
                  level="L"
                  includeMargin={false}
                />
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default UserDetails;
