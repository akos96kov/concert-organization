import React, { useEffect, useState } from 'react';
import queryString from 'query-string';

import { searchResource } from '../../rest/resource';

import BandCard from '../../components/Band/BandCard';
import EventCard from '../../components/Event/EventCard';

const SearchResult = (props) => {
  const [eventList, setEventList] = useState([]);
  const [bandList, setBandList] = useState([]);
  const [initialized, setInitialized] = useState(false);

  const searchText = queryString.parse(props.location.search).text;

  useEffect(() => {
    searchResource.getEventsAndBandsBySearchText(searchText).then((fetchedSearchResultList) => {
      setEventList(fetchedSearchResultList.events);
      setBandList(fetchedSearchResultList.bands);
      setInitialized(true);
    });
  }, [props.location.search]);

  return (
    <div className="main event-page">
      <div className="main-column search-result-page">
        {initialized && (
          <>
            {!!eventList.length && !!bandList.length && (
              <div className="result-title">{`Keresési találatok a "${searchText}" szóra:`}</div>
            )}
            {!!eventList.length && (
              <>
                <div className="result-block-title">Események:</div>
                {eventList.map((event) => (
                  <EventCard
                    onCardClick={() => props.history.push(`/event/${event.id}`)}
                    key={event.id}
                    name={event.name}
                    thumbnail={event.thumbnail}
                    date={event.date}
                    place={event.place}
                    description={event.description}
                  />
                ))}
              </>
            )}
            {!!bandList.length && (
              <>
                <div className="result-block-title">Együttesek:</div>
                {bandList.map((band) => (
                  <BandCard
                    key={band.id}
                    onCardClick={() => props.history.push(`/band/${band.id}`)}
                    name={band.name}
                    logo={band.logo && band.logo.path}
                  />
                ))}
              </>
            )}
            {eventList.length === 0 && bandList.length === 0 && (
              <div className="no-result">No result found for text: {searchText}</div>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default SearchResult;
