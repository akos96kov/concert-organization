import React from 'react';

const About = () => (
  <div className="main">
    <div className="main-column about-page">
      <div className="title">rólunk</div>
      <p>
        A koncertica egy zenei esemény kezelő alkalmazás. Adminként eseményeket lehet létrehozni és
        módosítani. A felhasználó láthatja ezeket az eseményeket, és regisztrálás majd bejelentkezés
        után jegyeket is vásárolhat, melyhez egy kosár nyújt segítséget. Az eseményen résztvevő
        együttesekről többet is meg lehet tudni az együttesek részelt oldalán (Az admin által ez is
        szerkeszthető). A keresővel eseményekre és együttesekre lehet keresni.
      </p>
      <p>
        Az alkalmazást Kovács Ákos (h660761) készítette szakdolgozat gyanánt. Ne telepítsük éles
        szerverre, csakis kizárólag localhost-on, saját gépünkön futtassuk!
      </p>
      <p>email: h660761@stud.u-szeged.hu</p>
    </div>
  </div>
);

export default About;
