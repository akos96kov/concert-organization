import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import DeleteIcon from '@material-ui/icons/Delete';

import Card from '../../components/Card/Card';
import Button from '../../components/Button/Button';
import { setCart } from '../../store/actions/userActions';
import { handleShowingBoughtTicketsModal } from '../../store/actions/modalActions';
import { ticketResource } from '../../rest/resource';

const countPrice = (tickets) => {
  let price = 0;

  tickets.forEach((newTicket) => {
    price += newTicket.ticketPrice * newTicket.quantityInCart;
  });

  return parseFloat(price.toFixed(2));
};

const Cart = ({ history }) => {
  const cart = useSelector((state) => state.user.cart);
  const [isCartSaveable, setIsCartSaveable] = useState(false);
  const [sumPrice, setSumPrice] = useState(countPrice(cart));
  const dispatch = useDispatch();

  useEffect(() => {
    setSumPrice(countPrice(cart));

    if (cart.length === 0) {
      history.replace('/');
    }
  }, [cart]);

  const saveTicketsToCart = () => {
    const ticketsToCart = [];

    cart.forEach((ticket) => {
      ticketsToCart.push({
        ticketId: ticket.ticketId,
        quantityInCart: ticket.quantityInCart,
        originalQuantityInCart: ticket.originalQuantityInCart,
      });
    });

    ticketResource.modifyTicketsInCart(ticketsToCart).then((newCart) => {
      dispatch(setCart(newCart));
      setIsCartSaveable(false);
    });
  };

  const handleTicketQuantiyChange = (e, ticket) => {
    if (e.target.value > ticket.quantity + ticket.originalQuantityInCart || e.target.value < 0) {
      return;
    }

    ticket.quantityInCart = e.target.value;

    const newCart = [...cart];
    const ticketIndex = cart.findIndex(
      (selectedTicket) => selectedTicket.ticketId === ticket.ticketId
    );
    newCart[ticketIndex] = ticket;

    dispatch(setCart(newCart));
    if (!isCartSaveable) {
      setIsCartSaveable(true);
    }
  };

  const handleDeleteTicket = (ticketId, quantity) => {
    ticketResource.deleteTicektFromCart(ticketId, quantity).then((newCart) => {
      dispatch(setCart(newCart));
    });
  };

  const handleBuyTickets = () => {
    const ticketsToBuy = [];

    cart.forEach((ticket) => {
      ticketsToBuy.push({
        ticketId: ticket.ticketId,
        quantityInCart: ticket.quantityInCart,
        originalQuantityInCart: ticket.originalQuantityInCart,
      });
    });

    ticketResource.buyTickets(ticketsToBuy).then((boughtTickets) => {
      dispatch(setCart([]));
      dispatch(handleShowingBoughtTicketsModal(boughtTickets));
      setIsCartSaveable(false);
    });
  };

  return (
    <div className="main cart-page">
      <div className="main-column">
        <div className="title">Kosár</div>
        <div className="ticket-list">
          {cart.map((ticket, index) => {
            return (
              <Card key={index}>
                <div className="ticket">
                  <div className="ticket-title-container">
                    <div className="event-name">{ticket.eventName}</div>
                    <div className="ticket-description">{ticket.ticketDescription}</div>
                  </div>
                  <div className="ticket-price">
                    <span className="price">{ticket.ticketPrice}$</span>
                    <input
                      type="number"
                      value={ticket.quantityInCart}
                      min={1}
                      max={ticket.quantity + ticket.originalQuantityInCart}
                      onChange={(e) => {
                        handleTicketQuantiyChange(e, ticket);
                      }}
                    />
                    <div className="ticket-delete-container">
                      <DeleteIcon
                        onClick={() =>
                          handleDeleteTicket(ticket.ticketId, ticket.originalQuantityInCart)
                        }
                      />
                    </div>
                  </div>
                </div>
              </Card>
            );
          })}
        </div>
        {sumPrice > 0 && <div className="ticket-sum-price">${sumPrice}</div>}
        <div className="cart-action-container">
          <Button contained onClick={handleBuyTickets}>
            Megvesz
          </Button>
          <Button disabled={!isCartSaveable} onClick={saveTicketsToCart}>
            Mentés
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Cart;
