import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import Button from '../../components/Button/Button';

import { ticketResource } from '../../rest/resource';

const TicketStatus = ({
  match: {
    params: { code },
  },
}) => {
  const user = useSelector((state) => state.user.user);

  if (!user || (user && user.role !== 'CONDUCTOR')) {
    return <Redirect to="/" />;
  }

  const [ticketStatus, setTicketStatus] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    ticketResource
      .getTicketStatus(code)
      .then((fetchedTicketStatus) => {
        setTicketStatus(fetchedTicketStatus);
      })
      .catch((err) => {
        setError(err);
      });
  }, []);

  if (error) {
    return (
      <div className="main ticket-status-page">
        <div className="main-column">
          <div className="ticket-status-container">
            <div className="ticket-label error">Valami hiba történt</div>
          </div>
        </div>
      </div>
    );
  }

  const activateTicket = () => {
    ticketResource
      .activateTicket(code)
      .then(() => setTicketStatus('USED'))
      .catch((err) => setError(err));
  };

  return (
    <div className="main ticket-status-page">
      <div className="main-column">
        {ticketStatus && (
          <div className="ticket-status-container">
            {ticketStatus === 'ACTIVE' ? (
              <>
                <div className="ticket-label">
                  A jegy érvényes, és <span className="active">AKTÍV</span> állapotban van
                </div>
                <Button contained onClick={activateTicket}>
                  Felhasznál
                </Button>
              </>
            ) : (
              <div className="ticket-label">A jegy már fel lett használva</div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default TicketStatus;
