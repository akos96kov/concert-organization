import React, { useState, useEffect } from 'react';

import { eventResource } from '../../rest/resource';

import EventCard from '../../components/Event/EventCard';

const EventList = ({ history }) => {
  const [events, setEvents] = useState([]);

  useEffect(() => {
    eventResource
      .getEvents()
      .then((fetchedEvents) => {
        setEvents(fetchedEvents);
      })
      .catch(() => {
        history.replace('/');
      });
  }, []);

  return (
    <div className="main">
      <div className="main-column">
        {events.length > 0 &&
          events.map((event) => (
            <EventCard
              onCardClick={() => history.push(`/event/${event.id}`)}
              key={event.id}
              name={event.name}
              thumbnail={event.thumbnail}
              date={event.date}
              place={event.place}
              description={event.description}
            />
          ))}
      </div>
    </div>
  );
};

export default EventList;
