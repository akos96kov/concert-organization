import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import EventCard from '../../../components/Event/EventCard';
import { eventResource } from '../../../rest/resource';
import Button from '../../../components/Button/Button';

const AdminEventList = ({ history }) => {
  const user = useSelector((state) => state.user.user);
  const [eventsForAdmin, setEventsForAdmin] = useState([]);

  if (!user || (user && user.role !== 'ADMIN')) {
    return <Redirect to="/" />;
  }

  useEffect(() => {
    eventResource
      .getEventsForAdmin()
      .then((fetchedEventsForAdmin) => setEventsForAdmin(fetchedEventsForAdmin));
  }, []);

  return (
    <div className="admin-event-list-page">
      {eventsForAdmin.length > 0 &&
        eventsForAdmin.map((event) => (
          <EventCard
            onCardClick={() => history.push(`/admin/event/${event.id}`)}
            key={event.id}
            name={event.name}
            thumbnail={event.thumbnail}
            date={event.date}
            place={event.place}
            description={event.description}
          />
        ))}
      <Button onClick={() => history.push('/admin/event/new')} contained>
        + új esemény
      </Button>
    </div>
  );
};

export default AdminEventList;
