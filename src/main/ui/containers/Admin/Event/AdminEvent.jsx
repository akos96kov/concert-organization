import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import Dropzone from 'react-dropzone';
import { useDispatch, useSelector } from 'react-redux';
import ClearIcon from '@material-ui/icons/Clear';
import DatePicker, { registerLocale } from 'react-datepicker';
import hu from 'date-fns/locale/hu';
import DeleteIcon from '@material-ui/icons/Delete';

import { toggleShowingInfoPopup } from '../../../store/actions/popupActions';
import Button from '../../../components/Button/Button';
import { eventResource, bandResource, placeResource } from '../../../rest/resource';
import InputField from '../../../components/InputField/InputField';

registerLocale('hu', hu);

const AdminEvent = ({ match, history }) => {
  const [eventId, setEventId] = useState('');
  const [eventName, setEventName] = useState('');
  const [eventNameError, setEventNameError] = useState(false);
  const [eventDescription, setEventDescription] = useState('');
  const [eventDate, setEventDate] = useState(new Date());
  const [dateError, setDateError] = useState(false);
  const [eventPlaceId, setEventPlaceId] = useState('');
  const [placeError, setPlaceError] = useState(false);
  const [thumbnail, setThumbnail] = useState({
    file: null,
    removePrevious: false,
    preview: null,
  });
  const [thumbnailError, setThumbnailError] = useState(false);
  const [eventBandIdList, setEventBandIdList] = useState([]);
  const [eventTicketList, setEventTicketList] = useState([]);
  const [ticketDescError, setTicketDescError] = useState(false);

  const [bands, setBands] = useState([]);
  const [places, setPlaces] = useState([]);
  const user = useSelector((state) => state.user.user);

  const [isReady, setIsReady] = useState(false);

  const dispatch = useDispatch();

  if (!user || (user && user.role !== 'ADMIN')) {
    return <Redirect to="/" />;
  }

  useEffect(() => {
    (async () => {
      const fetchedBands = await bandResource.getBands();
      const fetchedPlaces = await placeResource.getPlaces();

      if (match.params.id !== 'new') {
        const fetchedEvent = await eventResource.getEventForAdmin(match.params.id);

        setEventId(fetchedEvent.id);
        setEventName(fetchedEvent.name);
        setEventDate(fetchedEvent.date);
        setEventDescription(fetchedEvent.description || '');
        setEventPlaceId(fetchedEvent.placeId);
        setThumbnail((prevThumbnail) => {
          return { ...prevThumbnail, preview: `/media/${fetchedEvent.thumbnail}` };
        });
        setEventBandIdList(fetchedEvent.bandIdList);
        setEventTicketList(fetchedEvent.ticketList);
      } else {
        setEventPlaceId(fetchedPlaces[0].id);
      }

      setBands(fetchedBands);
      setPlaces(fetchedPlaces);
      setIsReady(true);
    })();
  }, []);

  if (!isReady) {
    return null;
  }

  const handleEventNameChange = (e) => {
    e.persist();
    if (eventNameError) {
      setEventNameError(false);
    }

    setEventName(e.target.value);
  };

  const handleEventDescriptionChange = (e) => {
    e.persist();
    setEventDescription(e.target.value);
  };

  const handleEventDateChange = (date) => {
    if (dateError) {
      setDateError(false);
    }

    setEventDate(date.toISOString());
  };

  const handlePlaceSelect = (e) => {
    e.persist();
    if (placeError) {
      setPlaceError(false);
    }

    setEventPlaceId(e.target.value);
  };

  const handleBandSelect = (e) => {
    e.persist();
    const selectedBandId = e.target.value;

    const newBandIdlist = [...eventBandIdList].filter((bandId) => bandId !== selectedBandId);

    if (newBandIdlist.length === eventBandIdList.length) {
      const newBand = bands.find((band) => band.id === selectedBandId);
      newBandIdlist.push(newBand.id);
    }

    setEventBandIdList(newBandIdlist);
  };

  const deleteBand = (selectedBandId) => {
    const newBandIdlist = [...eventBandIdList].filter((bandId) => bandId !== selectedBandId);

    setEventBandIdList(newBandIdlist);
  };

  const createImageObject = (file) => ({
    file: file || null,
    removePrevious: true,
    preview: file ? URL.createObjectURL(file) : null,
  });

  const onThumbnailDrop = (files) => {
    if (thumbnailError) {
      setThumbnailError(false);
    }
    const file = createImageObject(files[0]);
    setThumbnail(file);
  };

  const deleteThumbnail = () => {
    setThumbnail({
      file: null,
      removePrevious: true,
      preview: null,
    });
  };

  const handleTicketDescriptionChange = (e, ticketIndex) => {
    e.persist();
    if (ticketDescError) {
      setTicketDescError(false);
    }

    const newTicketList = [...eventTicketList];
    newTicketList[ticketIndex] = { ...newTicketList[ticketIndex], description: e.target.value };

    setEventTicketList(newTicketList);
  };

  const handleTicketPriceChange = (e, ticketIndex) => {
    e.persist();
    const newTicketList = [...eventTicketList];
    newTicketList[ticketIndex] = { ...newTicketList[ticketIndex], price: e.target.value };

    setEventTicketList(newTicketList);
  };

  const handleTicketQuantityChange = (e, ticketIndex) => {
    e.persist();
    const newTicketList = [...eventTicketList];
    newTicketList[ticketIndex] = { ...newTicketList[ticketIndex], quantity: e.target.value };

    setEventTicketList(newTicketList);
  };

  const handleAddNewTicket = () => {
    const newTicketList = [...eventTicketList, { id: '', description: '', price: 1, quantity: 0 }];

    setEventTicketList(newTicketList);
  };

  const handleTicketDelete = (index) => {
    const newTicketList = [...eventTicketList].filter((_, i) => i !== index);

    setEventTicketList(newTicketList);
  };

  const handleSaveEvent = async () => {
    let error = false;

    if (!eventName) {
      error = true;
      setEventNameError(true);
    }

    if (new Date(eventDate) < new Date()) {
      error = true;
      setDateError(true);
    }

    if (!eventPlaceId) {
      error = true;
      setPlaceError(true);
    }

    if (!thumbnail.preview) {
      error = true;
      setThumbnailError(true);
    }

    eventTicketList.forEach((eventTicket) => {
      if (!eventTicket.description) {
        error = true;
        setTicketDescError(true);
      }
    });

    if (error) {
      dispatch(toggleShowingInfoPopup('Hiba a kitöltésben!', 'ERROR'));
    } else {
      const event = {
        name: eventName,
        date: eventDate,
        description: eventDescription === '' ? null : eventDescription,
        placeId: eventPlaceId,
        bandIdList: eventBandIdList,
        ticketList: eventTicketList,
        removeThumbnail: thumbnail.removePrevious,
      };

      if (match.params.id === 'new') {
        try {
          await eventResource.saveEvent(event, thumbnail.file);
          dispatch(toggleShowingInfoPopup('Sikeres esemény létrehozás!', 'SUCCESS'));
          history.replace('/admin/events');
        } catch (err) {
          dispatch(toggleShowingInfoPopup('Valami hiba történt!', 'ERROR'));
        }
      } else {
        event.id = eventId;

        try {
          await eventResource.modifyEvent(event, thumbnail.file);
          dispatch(toggleShowingInfoPopup('Sikeres esemény módosítás!', 'SUCCESS'));
          history.replace('/admin/events');
        } catch (err) {
          dispatch(toggleShowingInfoPopup('Valami hiba történt!', 'ERROR'));
        }
      }
    }
  };

  return (
    <div className="main admin-event-page">
      <div className="main-column">
        <div className="title">
          {match.params.id === 'new' ? 'Esemény létrehozása' : 'Esemény szerkesztése'}
        </div>

        <div className="block name-block">
          <div className="event-input-wrapper">
            <InputField
              value={eventName}
              title="Esemény név *"
              name="event-name"
              onChange={(e) => handleEventNameChange(e)}
              error={eventNameError}
            />
          </div>
        </div>

        <div className="block description-block">
          <div className="event-input-wrapper">
            <InputField
              value={eventDescription}
              title="Esemény leírás"
              name="event-description"
              onChange={(e) => handleEventDescriptionChange(e)}
            />
          </div>
        </div>

        <div className="block date-block">
          <div className="date-input-title">Dátum *</div>
          <DatePicker
            className={dateError ? 'error' : ''}
            selected={new Date(eventDate)}
            onChange={(date) => handleEventDateChange(date)}
            showTimeSelect
            locale="hu"
            dateFormat="yyyy-MM-dd hh:mm aa"
          />
        </div>

        <div className="block place-block">
          <div className="select-input-title">Hely *</div>
          <select
            value={places.filter((place) => place.id === eventPlaceId)[0].id}
            onChange={handlePlaceSelect}
          >
            {places.map((place) => (
              <option key={place.id} value={place.id}>
                {place.name}
              </option>
            ))}
          </select>
        </div>

        <div className="block thumbnail-block">
          <div className="image-wireframe" style={{ color: thumbnailError ? 'red' : '' }}>
            <Dropzone accept="image/*" multiple={false} onDrop={onThumbnailDrop}>
              {({ getRootProps, getInputProps }) => (
                <div className="dropzone-wrapper" {...getRootProps()}>
                  <input {...getInputProps()} />
                  <div
                    className={`drop-zone${thumbnailError ? ' error' : ''}`}
                    style={{
                      backgroundImage: thumbnail.preview ? `url(${thumbnail.preview})` : '',
                    }}
                  >
                    <div className="thumbnail-title">Borító *</div>
                  </div>
                </div>
              )}
            </Dropzone>
            {thumbnail.preview && <DeleteIcon className="delete-icon" onClick={deleteThumbnail} />}
          </div>
        </div>

        <div className="block band-block">
          <div className="select-input-title">Együttes</div>
          <select value="" onChange={handleBandSelect}>
            <option value="">Együttes választása</option>
            {bands.map((band) => (
              <option key={band.id} value={band.id}>
                {band.name}
              </option>
            ))}
          </select>
          <div className="band-container">
            {eventBandIdList.map((bandId) => (
              <div key={bandId} className="band-item">
                <span>{bands.find((band) => band.id === bandId).name}</span>
                <DeleteIcon className="band-remove-icon" onClick={() => deleteBand(bandId)} />
              </div>
            ))}
          </div>
        </div>

        <div className="block ticket-list-block">
          <div className="ticket-title">Jegyek</div>
          {eventTicketList.map((ticket, index) => (
            <div key={index} className="ticket-block">
              <div className="info-block">
                <div className="event-input-wrapper description-wrapper">
                  <InputField
                    error={!!(ticketDescError && !ticket.description.length)}
                    value={ticket.description}
                    title="Jegy leírás"
                    name="ticket-description"
                    onChange={(e) => handleTicketDescriptionChange(e, index)}
                  />
                </div>
                <div className="ticket-technical-block">
                  <div className="event-input-wrapper price-wrapper">
                    <InputField
                      value={ticket.price}
                      title="Jegy ár ($)"
                      name="ticket-price"
                      type="number"
                      min={1}
                      onChange={(e) => handleTicketPriceChange(e, index)}
                    />
                  </div>
                  <div className="event-input-wrapper">
                    <InputField
                      value={ticket.quantity}
                      title="Mennyiség"
                      name="ticket-quantity"
                      type="number"
                      min={0}
                      onChange={(e) => handleTicketQuantityChange(e, index)}
                    />
                  </div>
                </div>
              </div>
              <div className="delete-container">
                <Button contained onClick={() => handleTicketDelete(index)}>
                  <DeleteIcon />
                </Button>
              </div>
            </div>
          ))}
          <Button onClick={handleAddNewTicket}>+ JEGY</Button>
        </div>

        <div className="save-block">
          <Button onClick={handleSaveEvent} contained>
            Mentés
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AdminEvent;
