import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import Card from '../../../components/Card/Card';
import Button from '../../../components/Button/Button';
import { placeResource } from '../../../rest/resource';

const AdminPlaceList = ({ history }) => {
  const user = useSelector((state) => state.user.user);
  const [placesForAdmin, setPlacesForAdmin] = useState([]);

  if (!user || (user && user.role !== 'ADMIN')) {
    return <Redirect to="/" />;
  }

  useEffect(() => {
    placeResource
      .getPlaces()
      .then((fetchedPlacesForAdmin) => setPlacesForAdmin(fetchedPlacesForAdmin));
  }, []);

  return (
    <div className="admin-place-list-page">
      {placesForAdmin.map((place) => (
        <Card className="place-card" key={place.id} handleCardClick={() => history.push(`/admin/place/${place.id}`)}>
          {place.name}
        </Card>
      ))}
      <Button onClick={() => history.push('/admin/place/new')} contained>
        + új hely
      </Button>
    </div>
  );
};

export default AdminPlaceList;
