import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Map from '../../../components/Map/Map';
import InputField from '../../../components/InputField/InputField';
import Button from '../../../components/Button/Button';

import { toggleShowingInfoPopup } from '../../../store/actions/popupActions';
import { placeResource } from '../../../rest/resource';

const AdminPlace = ({ match, history }) => {
  const [placeId, setPlaceId] = useState('');
  const [placeName, setPlaceName] = useState('TIK');
  const [placeNameError, setPlaceNameError] = useState(false);
  const [placeAddress, setPlaceAddress] = useState('Szeged, Egyetem u. 2, 6722');
  const [placeLatitude, setPalceLatitude] = useState(46.24758833304752);
  const [placeLongitude, setPlaceLongitude] = useState(20.141206620905063);

  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();

  if (!user || (user && user.role !== 'ADMIN')) {
    return <Redirect to="/" />;
  }

  useEffect(() => {
    (async () => {
      if (match.params.id !== 'new') {
        const fetchedPlace = await placeResource.getPlaceById(match.params.id);

        setPlaceId(fetchedPlace.id);
        setPlaceName(fetchedPlace.name);
        setPlaceAddress(fetchedPlace.address);
        setPalceLatitude(fetchedPlace.latitude);
        setPlaceLongitude(fetchedPlace.longitude);
      }
    })();
  }, []);

  const handleMapClick = (latlng, address) => {
    setPalceLatitude(latlng.lat);
    setPlaceLongitude(latlng.lng);
    setPlaceAddress(address);
  };

  const handlePlaceNameChange = (e) => {
    setPlaceNameError(false);
    setPlaceName(e.target.value);
  };

  const savePlace = async () => {
    let error = false;

    if (!placeName) {
      error = true;
      setPlaceNameError(true);
    }

    if (error) {
      dispatch(toggleShowingInfoPopup('Hiba a kitöltésben!', 'ERROR'));
    } else {
      const place = {
        name: placeName,
        latitude: placeLatitude,
        longitude: placeLongitude,
        address: placeAddress,
      };

      if (match.params.id === 'new') {
        try {
          await placeResource.savePlace(place);
          dispatch(toggleShowingInfoPopup('Sikeres hely létrehozás!', 'SUCCESS'));
          history.replace('/admin/places');
        } catch (err) {
          dispatch(toggleShowingInfoPopup('Valami hiba történt!', 'ERROR'));
        }
      } else {
        place.id = placeId;
        try {
          await placeResource.modifyPlace(place);
          dispatch(toggleShowingInfoPopup('Sikeres hely módosítás!', 'SUCCESS'));
          history.replace('/admin/places');
        } catch (err) {
          dispatch(toggleShowingInfoPopup('Valami hiba történt!', 'ERROR'));
        }
      }
    }
  };

  return (
    <div className="main admin-place-page">
      <div className="main-column">
        <div className="title">
          {match.params.id === 'new' ? 'Hely létrehozása' : 'Hely szerkesztése'}
        </div>
        <div className="place-map-container">
          <Map position={[placeLatitude, placeLongitude]} onMapClick={handleMapClick} />
        </div>
        <div className="address-block">{placeAddress}</div>
        <div className="place-input-wrapper">
          <InputField
            value={placeName}
            title="Hely neve *"
            name="place-name"
            onChange={(e) => handlePlaceNameChange(e)}
            error={placeNameError}
          />
        </div>
        <div className="save-block">
          <Button onClick={savePlace} contained>
            Mentés
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AdminPlace;
