import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import BandCard from '../../../components/Band/BandCard';
import Button from '../../../components/Button/Button';
import { bandResource } from '../../../rest/resource';

const AdminBandList = ({ history }) => {
  const user = useSelector((state) => state.user.user);
  const [bandsForAdmin, setBandsForAdmin] = useState([]);

  if (!user || (user && user.role !== 'ADMIN')) {
    return <Redirect to="/" />;
  }

  useEffect(() => {
    bandResource.getBands().then((fetchedBandsForAdmin) => setBandsForAdmin(fetchedBandsForAdmin));
  }, []);

  return (
    <div className="admin-event-list-page">
      {bandsForAdmin.map((band) => (
        <BandCard
          key={band.id}
          onCardClick={() => history.push(`/admin/band/${band.id}`)}
          name={band.name}
          logo={band.logo && band.logo.path}
        />
      ))}
      <Button onClick={() => history.push('/admin/band/new')} contained>
        + új együttes
      </Button>
    </div>
  );
};

export default AdminBandList;
