import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import Dropzone from 'react-dropzone';
import { useDispatch, useSelector } from 'react-redux';
import DeleteIcon from '@material-ui/icons/Delete';

import InputField from '../../../components/InputField/InputField';
import Button from '../../../components/Button/Button';

import { toggleShowingInfoPopup } from '../../../store/actions/popupActions';
import { bandResource } from '../../../rest/resource';

const AdminBand = ({ match, history }) => {
  const [bandId, setBandId] = useState('');
  const [bandName, setBandName] = useState('');
  const [bandNameError, setBandNameError] = useState(false);
  const [bandDescription, setBandDescription] = useState('');
  const [logo, setLogo] = useState({
    file: null,
    removePrevious: false,
    preview: null,
  });

  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();

  if (!user || (user && user.role !== 'ADMIN')) {
    return <Redirect to="/" />;
  }

  useEffect(() => {
    (async () => {
      if (match.params.id !== 'new') {
        const fetchedBand = await bandResource.getBandForAdmin(match.params.id);

        setBandId(fetchedBand.id);
        setBandName(fetchedBand.name);
        setBandDescription(fetchedBand.description);
        if (fetchedBand.logo) {
          setLogo((prevLogo) => ({ ...prevLogo, preview: `/media/${fetchedBand.logo}` }));
        }
      }
    })();
  }, []);

  const handleBandNameChange = (e) => {
    setBandNameError(false);
    setBandName(e.target.value);
  };

  const handleBandDescriptionChange = (e) => {
    setBandDescription(e.target.value);
  };

  const createImageObject = (file) => ({
    file: file || null,
    removePrevious: true,
    preview: file ? URL.createObjectURL(file) : null,
  });

  const onLogoDrop = (files) => {
    const file = createImageObject(files[0]);
    setLogo(file);
  };

  const deleteLogo = () => {
    setLogo({
      file: null,
      removePrevious: true,
      preview: null,
    });
  };

  const saveBand = async () => {
    let error = false;

    if (!bandName) {
      error = true;
      setBandNameError(true);
    }

    if (error) {
      dispatch(toggleShowingInfoPopup('Hiba a kitöltésben!', 'ERROR'));
    } else {
      const band = {
        name: bandName,
        description: bandDescription === '' ? null : bandDescription,
        removeLogo: logo.removePrevious,
      };

      if (match.params.id === 'new') {
        try {
          await bandResource.saveBand(band, logo.file);
          dispatch(toggleShowingInfoPopup('Sikeres együttes létrehozás!', 'SUCCESS'));
          history.replace('/admin/bands');
        } catch (err) {
          dispatch(toggleShowingInfoPopup('Valami hiba történt!', 'ERROR'));
        }
      } else {
        band.id = bandId;
        try {
          await bandResource.modifyBand(band, logo.file);
          dispatch(toggleShowingInfoPopup('Sikeres együttes módosítás!', 'SUCCESS'));
          history.replace('/admin/bands');
        } catch (err) {
          dispatch(toggleShowingInfoPopup('Valami hiba történt!', 'ERROR'));
        }
      }
    }
  };

  return (
    <div className="main admin-band-page">
      <div className="main-column">
        <div className="title">
          {match.params.id === 'new' ? 'Együttes létrehozása' : 'Együttes szerkesztése'}
        </div>

        <div className="band-input-wrapper">
          <InputField
            value={bandName}
            title="Együttes neve *"
            name="band-name"
            onChange={(e) => handleBandNameChange(e)}
            error={bandNameError}
          />
        </div>

        <div className="band-input-wrapper">
          <InputField
            value={bandDescription || ''}
            title="Leírás"
            name="band-description"
            onChange={(e) => handleBandDescriptionChange(e)}
          />
        </div>

        <div className="logo-block">
          <div className="image-wireframe">
            <Dropzone accept="image/*" multiple={false} onDrop={onLogoDrop}>
              {({ getRootProps, getInputProps }) => (
                <div className="dropzone-wrapper" {...getRootProps()}>
                  <input {...getInputProps()} />
                  <div
                    className="drop-zone"
                    style={{
                      backgroundImage: logo.preview ? `url(${logo.preview})` : '',
                    }}
                  >
                    <div className="logo-title">Borító</div>
                  </div>
                </div>
              )}
            </Dropzone>
            {logo.preview && <DeleteIcon className="delete-icon" onClick={deleteLogo} />}
          </div>
        </div>

        <div className="save-block">
          <Button onClick={saveBand} contained>
            Mentés
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AdminBand;
