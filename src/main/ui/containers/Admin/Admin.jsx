import React from 'react';
import { Route } from 'react-router-dom';

import AdminTab from '../../components/AdminTab/AdminTab';
import AdminEventList from './Event/AdminEventList';
import AdminBandList from './Band/AdminBandList';
import AdminPlaceList from './Place/AdminPlaceList';

const Admin = () => {
  return (
    <div className="main admin-event-list-page">
      <div className="main-column">
        <AdminTab />
        <Route path="/admin/events" component={AdminEventList} />
        <Route path="/admin/bands" component={AdminBandList} />
        <Route path="/admin/places" component={AdminPlaceList} />
      </div>
    </div>
  );
};

export default Admin;
