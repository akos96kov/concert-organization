import React, { useState, useEffect } from 'react';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { useDispatch, useSelector } from 'react-redux';

import Card from '../../components/Card/Card';
import Map from '../../components/Map/Map';
import Button from '../../components/Button/Button';
import { eventResource, ticketResource } from '../../rest/resource';
import { setCart } from '../../store/actions/userActions';
import { handleShowingEntryModal } from '../../store/actions/modalActions';
import { dateFormatter } from '../../utils/commonFunctions';

const Event = ({ match: { params }, history }) => {
  const [event, setEvent] = useState();
  const [tickets, setTickets] = useState([]);
  const [showTickets, setShowTickets] = useState(false);
  const [sumPrice, setSumPrice] = useState(0);
  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();

  const fetchEvent = () => {
    eventResource
      .getEventById(params.id)
      .then((fetchedEvent) => {
        const ticketList = fetchedEvent.ticketList.map((currentTicket) => ({
          ...currentTicket,
          buy: 0,
        }));
        setEvent(fetchedEvent);
        setTickets(ticketList);
      })
      .catch(() => {
        history.replace('/');
      });
  };

  useEffect(() => {
    fetchEvent();
  }, []);

  if (!event) {
    return null;
  }

  const increasePrice = (e, ticket) => {
    if (e.target.value > ticket.quantity || e.target.value < 0) {
      return;
    }

    ticket.buy = e.target.value;

    const newTickets = [...tickets];
    const ticketIndex = tickets.findIndex((selectedTicket) => selectedTicket.id === ticket.id);
    newTickets[ticketIndex] = ticket;

    let countPrice = 0;
    newTickets.forEach((newTicket) => {
      countPrice += newTicket.price * newTicket.buy;
    });

    setSumPrice(parseFloat(countPrice.toFixed(2)));
    setTickets(newTickets);
  };

  const addTicketsToCart = () => {
    const ticketsToBuy = [];

    tickets.forEach((ticket) => {
      if (ticket.buy > 0) {
        ticketsToBuy.push({ ticketId: ticket.id, quantity: ticket.buy });
      }
    });

    ticketResource.addTicketsToCart(ticketsToBuy).then((cart) => {
      dispatch(setCart(cart));
      setSumPrice(0);
      fetchEvent();
    });
  };

  return (
    <div className="main event-page">
      <div className="main-column">
        <div className="event-container">
          <div className="event-thumbnail-container">
            <div
              className="event-thumbnail-blur"
              style={{ backgroundImage: `url(/media/${event.thumbnail.path})` }}
            />
            <img
              src={`/media/${event.thumbnail.path}`}
              className="event-thumbnail"
              alt="event-thumbnail"
            />
          </div>
          <div className="event-title">{event.name}</div>
          <div className="date-place-block">
            {event.place.name} / {dateFormatter(event.date)}
          </div>
          <div className="event-band-block">
            {event.bandList.map((band, index) => (
              <div
                key={band.id}
                tabIndex="0"
                role="button"
                className="band-item"
                onClick={() => history.push(`/band/${band.id}`)}
              >
                {band.name}
                {index !== event.bandList.length - 1 ? ',' : ''}
              </div>
            ))}
          </div>
          <div className="event-description">{event.description}</div>
          <div className="exact-time-container">Kapunyitás: {dateFormatter(event.date, true)}</div>
          <div className="event-map-container">
            <Map position={[event.place.latitude, event.place.longitude]} />
          </div>
          <div className="event-address-container">{event.place.address}</div>
        </div>
        <div className="ticket-container">
          {event.ticketList.length > 0 && (
            <div className="ticket-container-title">
              <span className="ticket-title">JEGYEK</span>
              {showTickets ? (
                <ArrowDropUpIcon onClick={() => setShowTickets(false)} />
              ) : (
                <ArrowDropDownIcon onClick={() => setShowTickets(true)} />
              )}
            </div>
          )}
          {showTickets && (
            <>
              <div className="ticket-list">
                {tickets.map((ticket) => (
                  <Card key={ticket.id}>
                    <div className="ticket">
                      <span className="ticket-description">{ticket.description}</span>
                      <div className="ticket-price">
                        <span className="price">${ticket.price}</span>
                        <input
                          type="number"
                          value={ticket.buy}
                          min={0}
                          max={ticket.quantity}
                          onChange={(e) => increasePrice(e, ticket)}
                        />
                      </div>
                    </div>
                  </Card>
                ))}
              </div>
              {sumPrice > 0 && <div className="ticket-sum-price">${sumPrice}</div>}
              <Button contained onClick={addTicketsToCart} disabled={sumPrice < 0.1 || !user}>
                KOSÁRBA
              </Button>
              {!user && (
                <span
                  role="button"
                  tabIndex="0"
                  onClick={() => dispatch(handleShowingEntryModal(true))}
                  className="login-text"
                >
                  Jelentkezz be!
                </span>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Event;
