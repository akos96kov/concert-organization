import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';

import { eventResource } from '../../rest/resource';

const Home = ({ history }) => {
  const [forthCompingEvents, setForthComingEvents] = useState();

  useEffect(() => {
    eventResource.getForthComingEvents().then((fetchedForthCompingEvents) => {
      setForthComingEvents(fetchedForthCompingEvents);
    });
  }, []);

  return (
    <div className="main home-page">
      <div className="main-column">
        {forthCompingEvents && forthCompingEvents.length > 0 && (
          <div className="home-event-box">
            <Slider
              autoplay
              autoplaySpeed={5000}
              arrows={false}
              swipe={false}
              dots
              lazyLoad
              infinite
              speed={500}
              slidesToShow={1}
              slidesToScroll={1}
            >
              {forthCompingEvents.map((event) => {
                return (
                  <div
                    tabIndex="0"
                    role="button"
                    className="home-event-thumbnail-container"
                    key={event.id}
                    onClick={() => history.push(`/event/${event.id}`)}
                  >
                    <div
                      className="home-event-thumbnail-background"
                      style={{ backgroundImage: `url("/media/${event.thumbnail.path}")` }}
                    />
                    <img
                      className="home-event-thumbnail"
                      src={`/media/${event.thumbnail.path}`}
                      alt={event.name}
                    />
                  </div>
                );
              })}
            </Slider>
          </div>
        )}
        <div className="home-description-box">
          <p>Koncertica - az összes zenei esemény egy helyen!</p>
          <p>
            Válogass a számodra tetsző események közül és vásárold meg rájuk a számodra szimpatikus
            jegyeket! <br />
            Ha még nem tetted meg, regisztrálj vagy lépj be!
          </p>
        </div>
      </div>
    </div>
  );
};

export default Home;
