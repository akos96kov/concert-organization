import React, { useState, useEffect } from 'react';

import { bandResource } from '../../rest/resource';

import BandCard from '../../components/Band/BandCard';

const Bands = ({ history }) => {
  const [bands, setBands] = useState([]);

  useEffect(() => {
    bandResource
      .getBands()
      .then((fetchedBands) => {
        setBands(fetchedBands);
      })
      .catch(() => {
        history.replace('/');
      });
  }, []);

  return (
    <div className="main bands-page">
      <div className="main-column">
        {bands.length > 0 &&
          bands.map((band) => (
            <BandCard
              key={band.id}
              onCardClick={() => history.push(`/band/${band.id}`)}
              name={band.name}
              logo={band.logo && band.logo.path}
            />
          ))}
      </div>
    </div>
  );
};

export default Bands;
