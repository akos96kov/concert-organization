import React from 'react';

const Card = ({ children, className, handleCardClick }) => (
  <div role="button" tabIndex="0" onClick={handleCardClick} className={`card ${className}`}>
    {children}
  </div>
);

export default Card;
