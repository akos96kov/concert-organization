import React from 'react';
import PlaceIcon from '@material-ui/icons/Place';
import CalendarIcon from '@material-ui/icons/CalendarToday';

import Card from '../Card/Card';

const EventCard = ({ name, onCardClick, thumbnail, date, place, description }) => {
  let formattedDate = new Date(date);

  formattedDate = `${formattedDate.getFullYear()}-${formattedDate.getMonth() < 9 ? '0' : ''}${
    formattedDate.getMonth() + 1
  }-${formattedDate.getDate() < 10 ? '0' : ''}${formattedDate.getDate()}`;

  return (
    <Card className="event-card" handleCardClick={onCardClick}>
      <div
        className="event-thumbnail"
        style={{ backgroundImage: `url("/media/${thumbnail.path}")` }}
      />
      <div className="event-details">
        <div className="event-name">{name}</div>
        <div className="event-place-container">
          <PlaceIcon className="event-place-icon" />
          <div className="event-place">{place.address}</div>
        </div>
        <div className="event-date-container">
          <CalendarIcon className="event-date-icon" />
          <div className="event-date">{formattedDate}</div>
        </div>
      </div>
      <div className="event-description">{description}</div>
    </Card>
  );
};

export default EventCard;
