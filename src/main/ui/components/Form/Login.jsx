import React, { useState } from 'react';
import Alert from '@material-ui/lab/Alert';
import { useDispatch } from 'react-redux';

import Button from '../Button/Button';
import InputField from '../InputField/InputField';
import { userResource } from '../../rest/resource';
import { setUserIfAuthenticated } from '../../store/actions/userActions';
import { handleShowingEntryModal } from '../../store/actions/modalActions';
import { toggleShowingInfoPopup } from '../../store/actions/popupActions';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isAuthenticationFailed, setIsAuthenticationFailed] = useState(false);
  const dispatch = useDispatch();

  const fields = [
    {
      name: 'username',
      title: 'felhasználónév',
      value: username,
    },
    {
      name: 'password',
      title: 'jelszó',
      value: password,
      type: 'password',
    },
  ];

  const handleInputFieldChange = (event) => {
    const {
      target: { name, value },
    } = event;

    switch (name) {
      case 'username':
        return setUsername(value);
      case 'password':
        return setPassword(value);
      default:
        return null;
    }
  };

  const handleLogin = () => {
    userResource
      .authenticateUser({ username, password })
      .then((response) => {
        localStorage.setItem('authorization', response.token);
        dispatch(handleShowingEntryModal(false));
        dispatch(toggleShowingInfoPopup('Sikeres bejelentkezés', 'SUCCESS'));
        dispatch(setUserIfAuthenticated());
      })
      .catch((error) => {
        if (error.response.status === 403) {
          setIsAuthenticationFailed(true);
        }
      });
  };

  return (
    <div className="form-container">
      {fields.map((field) => {
        return (
          <InputField
            key={field.name}
            name={field.name}
            title={field.title}
            value={field.value}
            type={field.type}
            onChange={handleInputFieldChange}
          />
        );
      })}
      {isAuthenticationFailed && (
        <Alert className="form-error" severity="error">
          Helytelen felhasználónév vagy jelszó!
        </Alert>
      )}
      <div className="form-action-block">
        <Button contained onClick={handleLogin}>
          Belépés
        </Button>
      </div>
    </div>
  );
};

export default Login;
