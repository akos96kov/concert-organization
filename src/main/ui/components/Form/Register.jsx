import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import Button from '../Button/Button';
import InputField from '../InputField/InputField';
import { userResource } from '../../rest/resource';
import { handleShowingEntryModal } from '../../store/actions/modalActions';
import { toggleShowingInfoPopup } from '../../store/actions/popupActions';

const Register = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');

  const dispatch = useDispatch();

  const fields = [
    {
      name: 'name',
      title: 'teljes név',
      value: name,
    },
    {
      name: 'email',
      title: 'e-mail cím',
      value: email,
    },
    {
      name: 'username',
      title: 'felhasználónév',
      value: username,
    },
    {
      name: 'password',
      title: 'jelszó',
      value: password,
      type: 'password',
    },
    {
      name: 'passwordConfirm',
      title: 'jelszó még egyszer',
      value: passwordConfirm,
      type: 'password',
    },
  ];

  const handleInputFieldChange = (event) => {
    const { target } = event;

    switch (target.name) {
      case 'name':
        return setName(target.value);
      case 'email':
        return setEmail(target.value);
      case 'username':
        return setUsername(target.value);
      case 'password':
        return setPassword(target.value);
      case 'passwordConfirm':
        return setPasswordConfirm(target.value);
      default:
        return null;
    }
  };

  const handleRegister = () => {
    userResource
      .register({ name, email, username, password, passwordConfirm })
      .then(() => {
        dispatch(handleShowingEntryModal(false));
        dispatch(toggleShowingInfoPopup('Sikeres regisztráció!', 'SUCCESS'));
      })
      .catch(() => {
        dispatch(handleShowingEntryModal(false));
        dispatch(toggleShowingInfoPopup('A regisztráció nem sikerült!', 'ERROR'));
      });
  };

  return (
    <div className="form-container">
      {fields.map((field) => {
        return (
          <InputField
            key={field.name}
            name={field.name}
            title={field.title}
            value={field.value}
            type={field.type}
            onChange={handleInputFieldChange}
          />
        );
      })}
      <div className="form-action-block">
        <Button contained onClick={handleRegister}>
          Regisztráció
        </Button>
      </div>
    </div>
  );
};

export default Register;
