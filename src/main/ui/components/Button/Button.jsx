import React from 'react';
import MatButton from '@material-ui/core/Button/Button';

const Button = ({ children, onClick, contained, disabled }) => {
  return (
    <MatButton
      variant="outlined"
      className={`cor-button${disabled ? ' disabled' : ''}${contained ? ' contained' : ''}`}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </MatButton>
  );
};

export default Button;
