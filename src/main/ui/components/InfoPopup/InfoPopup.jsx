import React, { useEffect } from 'react';
import SuccessIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Cancel';
import WarningIcon from '@material-ui/icons/Error';
import { useSelector, useDispatch } from 'react-redux';
import Fade from '@material-ui/core/Fade';

import { toggleShowingInfoPopup } from '../../store/actions/popupActions';

const InfoPopup = () => {
  const dispatch = useDispatch();
  const popupContent = useSelector((state) => state.popup.popupContent);

  useEffect(() => {
    if (popupContent) {
      setTimeout(() => {
        dispatch(toggleShowingInfoPopup());
      }, 5000);
    }
  }, [popupContent]);

  return (
    <>
      {popupContent && (
        <Fade in timeout={1000} unmountOnExit>
          <div className="info-popup-container">
            {popupContent.status === 'SUCCESS' && (
              <SuccessIcon className="info-popup-icon success" />
            )}
            {popupContent.status === 'WARNING' && (
              <WarningIcon className="info-popup-icon warning" />
            )}
            {popupContent.status === 'ERROR' && <ErrorIcon className="info-popup-icon error" />}
            <span className="info-popup-text">{popupContent.content}</span>
          </div>
        </Fade>
      )}
    </>
  );
};

export default InfoPopup;
