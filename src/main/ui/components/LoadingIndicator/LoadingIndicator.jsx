import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useSelector } from 'react-redux';

const LoadingIndicator = () => {
  const isShowLoadingIndicator = useSelector(
    (state) => state.loadingIndicator.isShowLoadingIndicator
  );

  return (
    <>
      {isShowLoadingIndicator && (
        <div className="loading-indicator-container">
          <CircularProgress />
        </div>
      )}
    </>
  );
};

export default LoadingIndicator;
