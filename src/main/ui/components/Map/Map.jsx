import React, { useRef } from 'react';
import Leaflet from 'leaflet';
import { Map as LeafletMap, Marker, TileLayer } from 'react-leaflet';
// eslint-disable-next-line no-unused-vars
import LCG from 'leaflet-control-geocoder';

const zoom = 14;

const markerIcon = new Leaflet.Icon({
  iconUrl: '/assets/icons/marker.svg',
  iconSize: [36, 36],
  iconAnchor: [18, 36],
});

const Map = ({ position, onMapClick }) => {
  const mapRef = useRef(null);

  const handleMapClick = (e) => {
    if (onMapClick) {
      const map = mapRef.current.leafletElement;
      const geocoder = Leaflet.Control.Geocoder.nominatim();

      geocoder.reverse(e.latlng, map.options.crs.scale(map.getZoom()), (results) => {
        const result = results[0];
        if (result) {
          onMapClick(e.latlng, result.name);
        }
      });
    }
  };

  return (
    position && (
      <LeafletMap center={position} zoom={zoom} onclick={handleMapClick} ref={mapRef}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        <Marker position={position} icon={markerIcon} />
      </LeafletMap>
    )
  );
};

export default Map;
