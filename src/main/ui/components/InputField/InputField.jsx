import React from 'react';

const InputField = ({ onChange, value, name, title, type, error, min }) => {
  return (
    <div className={`input-wrapper${error ? ' error' : ''}`}>
      <div className="input-title">{title}</div>
      <input
        value={value}
        onChange={onChange}
        name={name}
        className="input-field"
        type={type || 'text'}
        min={min}
      />
    </div>
  );
};

export default InputField;
