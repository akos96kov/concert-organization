import React from 'react';

import Card from '../Card/Card';

const BandCard = ({ onCardClick, name, logo }) => {
  return (
    <Card className="band-card" handleCardClick={onCardClick}>
      <span className="band-name">{name}</span>
      {logo && (
        <div className="band-logo-container">
          <img src={`/media/${logo}`} className="band-logo" alt="band-logo" />
        </div>
      )}
    </Card>
  );
};

export default BandCard;
