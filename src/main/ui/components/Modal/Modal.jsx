import React from 'react';
import Fade from '@material-ui/core/Fade';
import ModalMaterial from '@material-ui/core/Modal';

const Modal = ({ children, onClose, className }) => {
  return (
    <ModalMaterial
      className={`custom-modal-window${className ? ` ${className}` : ''}`}
      open
      disablePortal
      disableEnforceFocus
      disableAutoFocus
      onClose={onClose}
    >
      <Fade in>
        <div className="modal-paper">{children}</div>
      </Fade>
    </ModalMaterial>
  );
};

export default Modal;
