import React from 'react';
import { PDFViewer, Document, Page, Text, View, StyleSheet, Image } from '@react-pdf/renderer';

import { dateFormatter } from '../../utils/commonFunctions';
import Modal from './Modal';

const styles = StyleSheet.create({
  page: {
    marginTop: 20,
  },
  section: {
    margin: 30,
    color: '#000',
    textAlign: 'center',
  },
  eventName: {
    fontSize: 30,
  },
  ticketDate: {
    fontSize: 22,
  },
  eventDescription: {
    marginTop: 14,
    fontSize: 18,
  },
  qrCodeContainer: {
    width: '100%',
  },
  qrCode: {
    width: 120,
    marginLeft: 150,
  },
  ticketDescription: {
    marginTop: 5,
    marginLeft: 20,
  },
});

const ModalTicket = ({ onClose, ticket }) => {
  return (
    <Modal onClose={onClose}>
      <PDFViewer width={433} height={643}>
        <Document>
          <Page size="A5" style={styles.page}>
            <View style={styles.section}>
              <Text style={styles.eventName}>{ticket.eventName}</Text>
              <Text style={styles.ticketDate}>
                {ticket.placeName} / {dateFormatter(ticket.eventDate)}
              </Text>
              <Text style={styles.eventDescription}>{ticket.eventDescription}</Text>
            </View>
            <View style={styles.qrCodeContainer}>
              <Image style={styles.qrCode} source={{ uri: ticket.qrCode }} />
            </View>
            <Text style={styles.ticketDescription}>{ticket.ticketDescription}</Text>
          </Page>
        </Document>
      </PDFViewer>
    </Modal>
  );
};

export default ModalTicket;
