import React from 'react';
import ModalMaterial from '@material-ui/core/Modal';
import Fade from '@material-ui/core/Fade';
import { useSelector, useDispatch } from 'react-redux';

import { handleShowingBoughtTicketsModal } from '../../store/actions/modalActions';
import Button from '../Button/Button';
import Card from '../Card/Card';
import Modal from './Modal';

const ModalBoughtTickets = () => {
  const boughtTickets = useSelector((state) => state.modal.currentlyBoughtTickets);
  const dispatch = useDispatch();

  const handleClosingModal = () => {
    dispatch(handleShowingBoughtTicketsModal([]));
  };

  return (
    <>
      {boughtTickets.length > 0 && (
        <Modal onClose={handleClosingModal} className="bought-tickets-modal">
          <div className="modal-header">Vásárlás</div>
          <div className="modal-content">
            <div className="success-text">Sikeres vásárlás!</div>
            {boughtTickets.map((ticket) => (
              <Card>
                <div className="ticket">
                  <div className="ticket-title-container">
                    <div className="event-name">{ticket.eventName}</div>
                    <div className="ticket-description">{ticket.ticketDescription}</div>
                  </div>
                  <div className="quantity-container">X {ticket.quantity}</div>
                </div>
              </Card>
            ))}
          </div>
          <div className="modal-footer">
            <Button contained onClick={handleClosingModal}>
              Rendben
            </Button>
          </div>
        </Modal>
      )}
    </>
  );
};

export default ModalBoughtTickets;
