import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Login from '../Form/Login';
import Register from '../Form/Register';
import { handleShowingEntryModal } from '../../store/actions/modalActions';
import Modal from './Modal';

const ModalEntry = () => {
  const [isLoginSubpage, handleSubpage] = useState(true);
  const showEntryModal = useSelector((state) => state.modal.showEntryModal);
  const dispatch = useDispatch();

  return (
    <>
      {showEntryModal && (
        <Modal onClose={() => dispatch(handleShowingEntryModal(false))}>
          <div className="modal-header">{isLoginSubpage ? 'Bejelentkezés' : 'Regisztráció'}</div>
          <div className="modal-content">{isLoginSubpage ? <Login /> : <Register />}</div>
          <div className="modal-footer">
            {isLoginSubpage && <span>Nincs még profilod? </span>}
            <span
              className="modal-footer-link"
              onClick={() => handleSubpage(!isLoginSubpage)}
              role="button"
              tabIndex="0"
            >
              {isLoginSubpage ? 'Regisztrálj' : '<< Vissza a bejelentkezéshez'}
            </span>
          </div>
        </Modal>
      )}
    </>
  );
};

export default ModalEntry;
