import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import SearchIcon from '@material-ui/icons/Search';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import CartIcon from '@material-ui/icons/ShoppingCart';
import { Divider } from '@material-ui/core';

import { emptyUser } from '../../store/actions/userActions';
import { handleShowingEntryModal } from '../../store/actions/modalActions';
import { toggleShowingInfoPopup } from '../../store/actions/popupActions';

const Header = () => {
  const history = useHistory();
  const [showProfileMenu, setShowProfileMenu] = useState(false);
  const [searchText, setSearchText] = useState('');
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(emptyUser());
    dispatch(toggleShowingInfoPopup('Sikeres kijelentkezés!', 'SUCCESS'));
    localStorage.clear();
    history.replace('/');
  };

  const handleSearchFieldChange = (event) => {
    setSearchText(event.target.value);
  };

  return (
    <div className="header">
      <div className="header-content">
        <div className="header-logo-area">
          <img
            // eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role
            role="button"
            tabIndex="0"
            className="header-logo"
            src="/assets/images/logo.png"
            alt="logo"
            onClick={() => history.push('/')}
          />
        </div>
        <div className="header-tab-area">
          <div className="header-tab-element">
            <div
              className="header-tab-element-container"
              role="button"
              tabIndex="0"
              onClick={() => history.push('/events')}
            >
              Események
            </div>
          </div>
          <div className="header-tab-element">
            <div
              className="header-tab-element-container"
              role="button"
              tabIndex="0"
              onClick={() => history.push('/bands')}
            >
              Előadók
            </div>
          </div>
          <div className="header-tab-element">
            <div className="header-tab-element-container">Újság</div>
          </div>
          <div className="header-tab-element">
            <div
              className="header-tab-element-container"
              role="button"
              tabIndex="0"
              onClick={() => history.push('/about')}
            >
              Rólunk
            </div>
          </div>
          <div className="header-tab-element">
            <div
              role="button"
              tabIndex="0"
              onClick={user.user ? null : () => dispatch(handleShowingEntryModal(true))}
              onMouseEnter={() => setShowProfileMenu(true)}
              onMouseLeave={() => setShowProfileMenu(false)}
              className="header-tab-element-container"
            >
              <PersonOutlineIcon />
              <span>Profil</span>
            </div>
            {showProfileMenu && (
              <div
                onMouseEnter={() => setShowProfileMenu(true)}
                onMouseLeave={() => setShowProfileMenu(false)}
                className="header-menu header-profile-menu"
              >
                {user.user && (
                  <>
                    <div
                      role="button"
                      tabIndex="0"
                      onClick={() => history.push('/user')}
                      className="header-menu-element"
                    >
                      {user.user.username}
                    </div>
                    <Divider />
                    {user.user.role === 'ADMIN' && (
                      <>
                        <div
                          className="header-menu-element"
                          role="button"
                          tabIndex="0"
                          onClick={() => history.push('/admin/events')}
                        >
                          Admin funkciók
                        </div>
                        <Divider />
                      </>
                    )}
                    <div
                      role="button"
                      tabIndex="0"
                      onClick={handleLogout}
                      className="header-menu-element"
                    >
                      Kijelentkezés
                    </div>
                  </>
                )}
              </div>
            )}
          </div>
        </div>
        <div className="header-search-area">
          <div className="header-search">
            <input
              type="text"
              value={searchText}
              className="header-search-input"
              placeholder="Keresés..."
              onChange={(event) => handleSearchFieldChange(event)}
            />
            <button
              type="button"
              className="header-search-button"
              onClick={() => history.replace(`/search?text=${searchText}`)}
            >
              <SearchIcon />
            </button>
          </div>
        </div>
        {user.cart && user.cart.length > 0 && (
          <div
            className="header-cart-area"
            onClick={() => history.push('/cart')}
            tabIndex="0"
            role="button"
          >
            <CartIcon />
            <span className="cart-number">{user.cart.length}</span>
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
