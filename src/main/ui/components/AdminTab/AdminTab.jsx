import React from 'react';
import { useHistory, useParams } from 'react-router-dom';

const AdminTab = () => {
  const history = useHistory();
  const params = useParams();

  const currentParam = params[0];

  return (
    <div className="admin-tab">
      <div
        className={`tab-item ${currentParam === 'events' ? ' active' : ''}`}
        tabIndex="0"
        role="button"
        onClick={() => history.replace('/admin/events')}
      >
        Események
      </div>
      <div
        className={`tab-item ${currentParam === 'bands' ? ' active' : ''}`}
        tabIndex="0"
        role="button"
        onClick={() => history.replace('/admin/bands')}
      >
        Együttesek
      </div>
      <div
        className={`tab-item ${currentParam === 'places' ? ' active' : ''}`}
        tabIndex="0"
        role="button"
        onClick={() => history.replace('/admin/places')}
      >
        Helyek
      </div>
    </div>
  );
};

export default AdminTab;
