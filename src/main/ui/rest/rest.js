import axios from 'axios';

import store from '../store/store';
import { toggleLoadingIndicator } from '../store/actions/loadingIndicatorActions';

const api = axios.create({
  baseURL: '/api',
  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',
  withCredentials: true,
});

api.interceptors.request.use(
  (request) => {
    toggleLoadingIndicator(true)(store.dispatch);

    const jwtToken = localStorage.getItem('authorization');

    if (jwtToken) {
      request.headers.authorization = `Bearer ${jwtToken}`;
    }

    return request;
  },
  (error) => {
    toggleLoadingIndicator(true)(store.dispatch);

    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  (response) => {
    toggleLoadingIndicator(false)(store.dispatch);

    return response;
  },
  (error) => {
    toggleLoadingIndicator(false)(store.dispatch);

    return Promise.reject(error);
  }
);

export { api };
