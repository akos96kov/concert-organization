import { api } from '../rest';

const addTicketsToCart = (tickets) => {
  return api.post('/ticket/cart', tickets).then((response) => response.data);
};

const modifyTicketsInCart = (tickets) => {
  return api.put('/ticket/cart', tickets).then((response) => response.data);
};

const buyTickets = (tickets) => {
  return api.post('/ticket/buy', tickets).then((response) => response.data);
};

const deleteTicektFromCart = (ticketId, quantityInCart) => {
  return api
    .delete(`/ticket/cart/${ticketId}`, { params: { quantityInCart } })
    .then((response) => response.data);
};

const fetchBoughtTickets = () => {
  return api.get('/ticket').then((response) => response.data);
};

const getTicketStatus = (ticketCode) => {
  return api.get(`/ticket/activate/${ticketCode}`).then((response) => response.data);
};

const activateTicket = (ticketCode) => {
  return api.put(`/ticket/activate/${ticketCode}`).then((response) => response.data);
};

export default {
  addTicketsToCart,
  modifyTicketsInCart,
  buyTickets,
  deleteTicektFromCart,
  fetchBoughtTickets,
  getTicketStatus,
  activateTicket,
};
