import { api } from '../rest';

const getEvents = () => {
  return api.get('/event').then((response) => response.data);
};

const getEventsForAdmin = () => {
  return api.get('/event/admin').then((response) => response.data);
};

const getEventById = (eventId) => {
  return api.get(`/event/${eventId}`).then((response) => response.data);
};

const getEventForAdmin = (eventId) => {
  return api.get(`/event/${eventId}/admin`).then((response) => response.data);
};

const getForthComingEvents = () => {
  return api.get('/event/forthComing').then((response) => response.data);
};

const saveEvent = (event, eventThumbnail) => {
  const formData = new FormData();

  const eventData = [JSON.stringify(event)];
  formData.append('event', new Blob(eventData, { type: 'application/json' }));

  if (eventThumbnail && eventThumbnail.path && eventThumbnail.size) {
    formData.append('event-thumbnail', eventThumbnail);
  }

  return api.post('/event/admin', formData).then((response) => response.data);
};

const modifyEvent = (event, eventThumbnail) => {
  const formData = new FormData();

  const eventData = [JSON.stringify(event)];
  formData.append('event', new Blob(eventData, { type: 'application/json' }));

  if (eventThumbnail && eventThumbnail.path && eventThumbnail.size) {
    formData.append('event-thumbnail', eventThumbnail);
  }

  return api.put('/event/admin', formData).then((response) => response.data);
};

export default {
  getEvents,
  getEventById,
  getForthComingEvents,
  getEventsForAdmin,
  saveEvent,
  getEventForAdmin,
  modifyEvent,
};
