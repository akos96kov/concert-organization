import { api } from '../rest';

const getPlaces = () => {
  return api.get('/place').then((response) => response.data);
};

const getPlaceById = (id) => {
  return api.get(`/place/${id}/admin`).then((response) => response.data);
};

const savePlace = (place) => {
  return api.post(`/place/admin`, place).then((response) => response.data);
};

const modifyPlace = (place) => {
  return api.put(`/place/admin`, place).then((response) => response.data);
};

export default {
  getPlaces,
  getPlaceById,
  savePlace,
  modifyPlace,
};
