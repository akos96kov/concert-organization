import { api } from '../rest';

const authenticateUser = (user) => {
  return api.post('/authenticate', user).then((response) => response.data);
};

const isAuthenticated = () => {
  return api.get('/user').then((response) => {
    return response.data;
  });
};

const modifyUser = (user) => {
  return api.put('/user', user).then((response) => response.data);
};

const register = (user) => {
  return api.post('/user/register', user).then((response) => {
    return response.data;
  });
};

export default {
  authenticateUser,
  isAuthenticated,
  modifyUser,
  register,
};
