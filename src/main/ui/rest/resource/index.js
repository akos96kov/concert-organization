export { default as eventResource } from './eventResource';
export { default as userResource } from './userResource';
export { default as bandResource } from './bandResource';
export { default as ticketResource } from './ticketResource';
export { default as searchResource } from './searchResource';
export { default as placeResource } from './placeResource';
