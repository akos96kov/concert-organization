import { api } from '../rest';

const getEventsAndBandsBySearchText = (searchText) => {
  return api.get('/search', { params: { searchText } }).then((response) => response.data);
};

export default {
  getEventsAndBandsBySearchText,
};
