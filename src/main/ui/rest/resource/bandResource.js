import { api } from '../rest';

const getBand = (id) => {
  return api.get(`/band/${id}`).then((response) => response.data);
};

const getBands = () => {
  return api.get('/band').then((response) => response.data);
};

const getBandForAdmin = (id) => {
  return api.get(`/band/${id}/admin`).then((response) => response.data);
};

const saveBand = (band, logo) => {
  const formData = new FormData();

  const bandData = [JSON.stringify(band)];
  formData.append('band', new Blob(bandData, { type: 'application/json' }));

  if (logo && logo.path && logo.size) {
    formData.append('band-logo', logo);
  }

  return api.post('/band/admin', formData).then((response) => response.data);
};

const modifyBand = (band, logo) => {
  const formData = new FormData();

  const bandData = [JSON.stringify(band)];
  formData.append('band', new Blob(bandData, { type: 'application/json' }));

  if (logo && logo.path && logo.size) {
    formData.append('band-logo', logo);
  }

  return api.put('/band/admin', formData).then((response) => response.data);
};

export default {
  getBand,
  getBands,
  getBandForAdmin,
  saveBand,
  modifyBand,
};
