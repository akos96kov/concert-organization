import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';

import { setUserIfAuthenticated } from './store/actions/userActions';
import { pages } from './utils/pages';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import ModalEntry from './components/Modal/ModalEntry';
import ModalBoughtTickets from './components/Modal/ModalBoughtTickets';
import LoadingIndicator from './components/LoadingIndicator/LoadingIndicator';
import InfoPopup from './components/InfoPopup/InfoPopup';
import store from './store/store';

const App = () => {
  const [initalized, setInitialized] = useState(false);

  useEffect(() => {
    setUserIfAuthenticated()(store.dispatch)
      .then(() => {
        setInitialized(true);
      })
      .catch(() => {
        setInitialized(true);
      });
  }, []);

  if (!initalized) {
    return null;
  }

  return (
    <ReduxProvider store={store}>
      <Router>
        <Header />
        <Switch>
          {pages.map((page, index) => {
            return (
              <Route
                exact={page.exact}
                strict={page.strict}
                path={page.path}
                component={page.component}
                key={page.path + index}
              />
            );
          })}
        </Switch>
        <Footer />

        <LoadingIndicator />
        <ModalEntry />
        <ModalBoughtTickets />
        <InfoPopup />
      </Router>
    </ReduxProvider>
  );
};

export default App;
