const initialState = {
  isShowLoadingIndicator: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'TOGGLE_LOADING_INDICATOR':
      return {
        isShowLoadingIndicator: action.isShowLoadingIndicator,
      };
    default:
      return state;
  }
};
