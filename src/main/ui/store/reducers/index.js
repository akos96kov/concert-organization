import { combineReducers } from 'redux';

import modalReducer from './modalReducer';
import userReducer from './userReducer';
import loadingIndicatorReducer from './loadingIndicatorReducer';
import popupReducer from './popupReducer';

export default combineReducers({
  modal: modalReducer,
  user: userReducer,
  loadingIndicator: loadingIndicatorReducer,
  popup: popupReducer,
});
