const initialState = {
  popupContent: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'TOGGLE_SHOWING_INFO_POPUP':
      return {
        popupContent: action.popupContent,
      };
    default:
      return state;
  }
};
