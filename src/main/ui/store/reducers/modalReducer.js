const initialState = {
  showEntryModal: false,
  currentlyBoughtTickets: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'HANDLE_SHOWING_ENTRY_MODAL':
      return {
        ...state,
        showEntryModal: action.isShowEntryModal,
      };
    case 'HANDLE_SHOWING_BOUGHT_TICKETS_MODAL':
      return {
        ...state,
        currentlyBoughtTickets: action.tickets,
      };
    default:
      return state;
  }
};
