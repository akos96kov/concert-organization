const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER':
      // eslint-disable-next-line no-case-declarations
      const user = action.user
        ? {
            id: action.user.id,
            name: action.user.name,
            email: action.user.email,
            username: action.user.username,
            role: action.user.role
          }
        : null;

      return { ...state, user, cart: action.user ? action.user.eventUserTickets : null };
    case 'SET_CART':
      return { ...state, cart: action.cart };
    default:
      return state;
  }
};
