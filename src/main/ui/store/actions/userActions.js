import { userResource } from '../../rest/resource';

const setUserIfAuthenticated = () => {
  return (dispatch) => {
    return userResource.isAuthenticated().then((user) => {
      dispatch({
        type: 'SET_USER',
        user,
      });
    });
  };
};

const emptyUser = () => {
  return { type: 'SET_USER', user: undefined };
};

const setCart = (cart) => {
  return {
    type: 'SET_CART',
    cart,
  };
};

export { setUserIfAuthenticated, emptyUser, setCart };
