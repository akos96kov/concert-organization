const toggleShowingInfoPopup = (content, status) => {
  return {
    type: 'TOGGLE_SHOWING_INFO_POPUP',
    popupContent: content ? { content, status } : null,
  };
};

export { toggleShowingInfoPopup };
