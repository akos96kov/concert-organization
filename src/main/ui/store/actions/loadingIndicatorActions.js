const toggleLoadingIndicator = (isShowLoadingIndicator) => (dispatch) => {
  return dispatch({
    type: 'TOGGLE_LOADING_INDICATOR',
    isShowLoadingIndicator,
  });
};

export { toggleLoadingIndicator };
