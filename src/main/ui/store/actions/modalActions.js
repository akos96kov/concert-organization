const handleShowingEntryModal = (isShowEntryModal) => {
  return {
    type: 'HANDLE_SHOWING_ENTRY_MODAL',
    isShowEntryModal,
  };
};

const handleShowingBoughtTicketsModal = (tickets) => {
  return {
    type: 'HANDLE_SHOWING_BOUGHT_TICKETS_MODAL',
    tickets,
  };
};

export { handleShowingEntryModal, handleShowingBoughtTicketsModal };
