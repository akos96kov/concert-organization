export const dateFormatter = (date, setExactTime) => {
  const formattedDate = new Date(date);

  let dateAsString = `${formattedDate.getFullYear()}-${formattedDate.getMonth() < 9 ? '0' : ''}${
    formattedDate.getMonth() + 1
  }-${formattedDate.getDate() < 10 ? '0' : ''}${formattedDate.getDate()}`;

  if (setExactTime) {
    dateAsString = `${dateAsString} ${
      formattedDate.getHours() < 10 ? '0' : ''
    }${formattedDate.getHours()}:${
      formattedDate.getMinutes() < 10 ? '0' : ''
    }${formattedDate.getMinutes()}`;
  }

  return dateAsString;
};
