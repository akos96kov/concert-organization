import Band from '../containers/Band/Band';
import Bands from '../containers/Bands/Bands';
import Cart from '../containers/Cart/Cart';
import Event from '../containers/Event/Event';
import EventList from '../containers/EventList/EventList';
import Home from '../containers/Home/Home';
import SearchResult from '../containers/SearchResult/SearchResult';
import UserDetails from '../containers/User/UserDetails';
import About from '../containers/About/About';
import Admin from '../containers/Admin/Admin';
import AdminEvent from '../containers/Admin/Event/AdminEvent';
import AdminBand from '../containers/Admin/Band/AdminBand';
import AdminPlace from '../containers/Admin/Place/AdminPlace';
import TicketStatus from '../containers/TicketStatus/TicketStatus';

const pages = [
  {
    path: '/',
    component: Home,
    exact: true,
  },
  {
    path: '/events',
    component: EventList,
  },
  {
    path: '/event/:id',
    component: Event,
    exact: true,
  },
  {
    path: '/bands',
    component: Bands,
  },
  {
    path: '/band/:id',
    component: Band,
  },
  {
    path: '/cart',
    component: Cart,
  },
  {
    path: '/search',
    component: SearchResult,
  },
  {
    path: '/user',
    component: UserDetails,
  },
  {
    path: '/about',
    component: About,
  },
  {
    path: '/admin/(events|bands|places)',
    component: Admin,
  },
  {
    path: '/admin/event/:id',
    component: AdminEvent,
    exact: true,
  },
  {
    path: '/admin/band/:id',
    component: AdminBand,
    exact: true,
  },
  {
    path: '/admin/place/:id',
    component: AdminPlace,
    exact: true,
  },
  {
    path: '/ticket/:code',
    component: TicketStatus,
  },
];

export { pages };
