const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const SOURCE_PATH = path.join(__dirname, 'src', 'main', 'ui');
const TARGET_PATH = path.join(__dirname, 'target', 'classes', 'public');

module.exports = (env = {}) => {
  const isProduction = env.production;

  return {
    entry: [
      path.join(SOURCE_PATH, 'index.js'),
      path.join(SOURCE_PATH, 'assets', 'style', 'style.scss'),
    ],
    devtool: 'sourcemaps',
    cache: true,
    mode: 'development',
    performance: {
      hints: false,
    },
    output: {
      path: TARGET_PATH,
      publicPath: '/',
      filename: 'app.min.js',
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id].css',
      }),
      new HtmlWebpackPlugin({
        title: 'Concert Organization',
        favicon: path.join(SOURCE_PATH, 'trzalica.ico'),
      }),
      new CopyPlugin({
        patterns: [
          {
            from: path.join(SOURCE_PATH, 'assets', 'images'),
            to: path.join(TARGET_PATH, 'assets', 'images'),
          },
          {
            from: path.join(SOURCE_PATH, 'assets', 'icons'),
            to: path.join(TARGET_PATH, 'assets', 'icons'),
          },
          {
            from: path.join(SOURCE_PATH, 'assets', 'fonts'),
            to: path.join(TARGET_PATH, 'assets', 'fonts'),
          },
        ],
      }),
    ],
    module: {
      rules: [
        {
          // Handles react javascript
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env', '@babel/preset-react'],
                plugins: ['@babel/plugin-proposal-class-properties'],
              },
            },
          ],
        },
        {
          // Handles statc files which do not needs generation
          test: /\.(html|png|ttf|mp4|jpe?g)$/,
          exclude: /node_modules/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]?[hash]',
            context: 'assets',
          },
        },
        {
          // Handles the style files
          test: /\.s?css$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader?-url',
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: () => [
                  require('postcss-flexbugs-fixes'),
                  require('postcss-preset-env')({
                    autoprefixer: {
                      flexbox: 'no-2009',
                    },
                    stage: 3,
                  }),
                ],
                sourceMap: !isProduction,
              },
            },
            'sass-loader',
          ],
        },
      ],
    },
    watchOptions: {
      ignored: /node_modules/,
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: ['node_modules', SOURCE_PATH],
    },
  };
};
